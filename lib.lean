inductive Fuel
| empty : Fuel
| more : Fuel -> Fuel

@[export fuel_empty]
private abbrev _sym0 := Fuel.empty

@[export fuel_mk_more]
private abbrev _sym1 := Fuel.more

@[export get_some_fuel]
def get_some_fuel : Fuel := .more $ .more .empty
