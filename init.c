#include <lean/lean.h>

int init(void) {
    lean_initialize_runtime_module()
    //lean_initialize();  // necessary if you (indirectly) access the `Lean` package

    lean_object * res;
    // use same default as for Lean executables
    uint8_t builtin = 1;
    res = initialize_A_B(builtin, lean_io_mk_world());
    if (lean_io_result_is_ok(res)) {
        lean_dec_ref(res);
    } else {
        lean_io_result_show_error(res);
        lean_dec(res);
        return 1;
    }
    res = initialize_C(builtin, lean_io_mk_world());
    if (lean_io_result_is_ok(res)) {
        
    }

    lean_init_task_manager();  // necessary if you (indirectly) use `Task`
    lean_io_mark_end_initialization();
    return 0
}
