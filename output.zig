pub const __builtin_bswap16 = @import("std").zig.c_builtins.__builtin_bswap16;
pub const __builtin_bswap32 = @import("std").zig.c_builtins.__builtin_bswap32;
pub const __builtin_bswap64 = @import("std").zig.c_builtins.__builtin_bswap64;
pub const __builtin_signbit = @import("std").zig.c_builtins.__builtin_signbit;
pub const __builtin_signbitf = @import("std").zig.c_builtins.__builtin_signbitf;
pub const __builtin_popcount = @import("std").zig.c_builtins.__builtin_popcount;
pub const __builtin_ctz = @import("std").zig.c_builtins.__builtin_ctz;
pub const __builtin_clz = @import("std").zig.c_builtins.__builtin_clz;
pub const __builtin_sqrt = @import("std").zig.c_builtins.__builtin_sqrt;
pub const __builtin_sqrtf = @import("std").zig.c_builtins.__builtin_sqrtf;
pub const __builtin_sin = @import("std").zig.c_builtins.__builtin_sin;
pub const __builtin_sinf = @import("std").zig.c_builtins.__builtin_sinf;
pub const __builtin_cos = @import("std").zig.c_builtins.__builtin_cos;
pub const __builtin_cosf = @import("std").zig.c_builtins.__builtin_cosf;
pub const __builtin_exp = @import("std").zig.c_builtins.__builtin_exp;
pub const __builtin_expf = @import("std").zig.c_builtins.__builtin_expf;
pub const __builtin_exp2 = @import("std").zig.c_builtins.__builtin_exp2;
pub const __builtin_exp2f = @import("std").zig.c_builtins.__builtin_exp2f;
pub const __builtin_log = @import("std").zig.c_builtins.__builtin_log;
pub const __builtin_logf = @import("std").zig.c_builtins.__builtin_logf;
pub const __builtin_log2 = @import("std").zig.c_builtins.__builtin_log2;
pub const __builtin_log2f = @import("std").zig.c_builtins.__builtin_log2f;
pub const __builtin_log10 = @import("std").zig.c_builtins.__builtin_log10;
pub const __builtin_log10f = @import("std").zig.c_builtins.__builtin_log10f;
pub const __builtin_abs = @import("std").zig.c_builtins.__builtin_abs;
pub const __builtin_fabs = @import("std").zig.c_builtins.__builtin_fabs;
pub const __builtin_fabsf = @import("std").zig.c_builtins.__builtin_fabsf;
pub const __builtin_floor = @import("std").zig.c_builtins.__builtin_floor;
pub const __builtin_floorf = @import("std").zig.c_builtins.__builtin_floorf;
pub const __builtin_ceil = @import("std").zig.c_builtins.__builtin_ceil;
pub const __builtin_ceilf = @import("std").zig.c_builtins.__builtin_ceilf;
pub const __builtin_trunc = @import("std").zig.c_builtins.__builtin_trunc;
pub const __builtin_truncf = @import("std").zig.c_builtins.__builtin_truncf;
pub const __builtin_round = @import("std").zig.c_builtins.__builtin_round;
pub const __builtin_roundf = @import("std").zig.c_builtins.__builtin_roundf;
pub const __builtin_strlen = @import("std").zig.c_builtins.__builtin_strlen;
pub const __builtin_strcmp = @import("std").zig.c_builtins.__builtin_strcmp;
pub const __builtin_object_size = @import("std").zig.c_builtins.__builtin_object_size;
pub const __builtin___memset_chk = @import("std").zig.c_builtins.__builtin___memset_chk;
pub const __builtin_memset = @import("std").zig.c_builtins.__builtin_memset;
pub const __builtin___memcpy_chk = @import("std").zig.c_builtins.__builtin___memcpy_chk;
pub const __builtin_memcpy = @import("std").zig.c_builtins.__builtin_memcpy;
pub const __builtin_expect = @import("std").zig.c_builtins.__builtin_expect;
pub const __builtin_nanf = @import("std").zig.c_builtins.__builtin_nanf;
pub const __builtin_huge_valf = @import("std").zig.c_builtins.__builtin_huge_valf;
pub const __builtin_inff = @import("std").zig.c_builtins.__builtin_inff;
pub const __builtin_isnan = @import("std").zig.c_builtins.__builtin_isnan;
pub const __builtin_isinf = @import("std").zig.c_builtins.__builtin_isinf;
pub const __builtin_isinf_sign = @import("std").zig.c_builtins.__builtin_isinf_sign;
pub const __has_builtin = @import("std").zig.c_builtins.__has_builtin;
pub const __builtin_assume = @import("std").zig.c_builtins.__builtin_assume;
pub const __builtin_unreachable = @import("std").zig.c_builtins.__builtin_unreachable;
pub const __builtin_constant_p = @import("std").zig.c_builtins.__builtin_constant_p;
pub const __builtin_mul_overflow = @import("std").zig.c_builtins.__builtin_mul_overflow;
pub const ptrdiff_t = c_long;
pub const wchar_t = c_int;
pub const max_align_t = extern struct {
    __clang_max_align_nonce1: c_longlong align(8),
    __clang_max_align_nonce2: c_longdouble align(16),
};
pub const int_least64_t = i64;
pub const uint_least64_t = u64;
pub const int_fast64_t = i64;
pub const uint_fast64_t = u64;
pub const int_least32_t = i32;
pub const uint_least32_t = u32;
pub const int_fast32_t = i32;
pub const uint_fast32_t = u32;
pub const int_least16_t = i16;
pub const uint_least16_t = u16;
pub const int_fast16_t = i16;
pub const uint_fast16_t = u16;
pub const int_least8_t = i8;
pub const uint_least8_t = u8;
pub const int_fast8_t = i8;
pub const uint_fast8_t = u8;
pub const intmax_t = c_long;
pub const uintmax_t = c_ulong;
pub extern fn lean_notify_assert(fileName: [*c]const u8, line: c_int, condition: [*c]const u8) void;
pub fn lean_is_big_object_tag(arg_tag: u8) callconv(.C) bool {
    var tag = arg_tag;
    return (((@bitCast(c_int, @as(c_uint, tag)) == @as(c_int, 246)) or (@bitCast(c_int, @as(c_uint, tag)) == @as(c_int, 247))) or (@bitCast(c_int, @as(c_uint, tag)) == @as(c_int, 248))) or (@bitCast(c_int, @as(c_uint, tag)) == @as(c_int, 249));
}
pub const assertion_failed___FILE___94 = [1]u8; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:114:14: warning: struct demoted to opaque type - has bitfield
pub const lean_object = opaque {};
pub const lean_obj_arg = ?*lean_object;
pub const b_lean_obj_arg = ?*lean_object;
pub const u_lean_obj_arg = ?*lean_object;
pub const lean_obj_res = ?*lean_object;
pub const b_lean_obj_res = ?*lean_object;
pub const lean_ctor_object = extern struct {
    m_header: lean_object align(8),
    pub fn m_objs(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object);
        return @ptrCast(ReturnType, @alignCast(@alignOf(?*lean_object), @ptrCast(Intermediate, self) + 8));
    }
};
pub const lean_array_object = extern struct {
    m_header: lean_object align(8),
    m_size: usize,
    m_capacity: usize,
    pub fn m_data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object);
        return @ptrCast(ReturnType, @alignCast(@alignOf(?*lean_object), @ptrCast(Intermediate, self) + 24));
    }
};
pub const lean_sarray_object = extern struct {
    m_header: lean_object align(8),
    m_size: usize,
    m_capacity: usize,
    pub fn m_data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        return @ptrCast(ReturnType, @alignCast(@alignOf(u8), @ptrCast(Intermediate, self) + 24));
    }
};
pub const lean_string_object = extern struct {
    m_header: lean_object align(8),
    m_size: usize,
    m_capacity: usize,
    m_length: usize,
    pub fn m_data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        return @ptrCast(ReturnType, @alignCast(@alignOf(u8), @ptrCast(Intermediate, self) + 32));
    }
};
pub const lean_closure_object = extern struct {
    m_header: lean_object align(8),
    m_fun: ?*anyopaque,
    m_arity: u16,
    m_num_fixed: u16,
    pub fn m_objs(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), ?*lean_object);
        return @ptrCast(ReturnType, @alignCast(@alignOf(?*lean_object), @ptrCast(Intermediate, self) + 24));
    }
};
pub const lean_ref_object = extern struct {
    m_header: lean_object,
    m_value: ?*lean_object,
}; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:195:28: warning: unsupported type: 'Atomic'
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:193:9: warning: struct demoted to opaque type - unable to translate type of field m_value
pub const lean_thunk_object = opaque {}; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:267:28: warning: unsupported type: 'Atomic'
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:199:8: warning: struct demoted to opaque type - unable to translate type of field m_value
pub const struct_lean_task = opaque {};
pub const lean_task_imp = extern struct {
    m_closure: ?*lean_object,
    m_head_dep: ?*struct_lean_task,
    m_next_dep: ?*struct_lean_task,
    m_prio: c_uint,
    m_canceled: u8,
    m_keep_alive: u8,
    m_deleted: u8,
};
pub const lean_task_object = struct_lean_task;
pub const lean_external_finalize_proc = ?*const fn (?*anyopaque) callconv(.C) void;
pub const lean_external_foreach_proc = ?*const fn (?*anyopaque, b_lean_obj_arg) callconv(.C) void;
pub const lean_external_class = extern struct {
    m_finalize: lean_external_finalize_proc,
    m_foreach: lean_external_foreach_proc,
};
pub extern fn lean_register_external_class(lean_external_finalize_proc, lean_external_foreach_proc) [*c]lean_external_class;
pub const lean_external_object = extern struct {
    m_header: lean_object,
    m_class: [*c]lean_external_class,
    m_data: ?*anyopaque,
};
pub fn lean_is_scalar(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return (@intCast(usize, @ptrToInt(o)) & @bitCast(c_ulong, @as(c_long, @as(c_int, 1)))) == @bitCast(c_ulong, @as(c_long, @as(c_int, 1)));
}
pub fn lean_box(arg_n: usize) callconv(.C) ?*lean_object {
    var n = arg_n;
    return @intToPtr(?*lean_object, (n << @intCast(@import("std").math.Log2Int(usize), 1)) | @bitCast(c_ulong, @as(c_long, @as(c_int, 1))));
}
pub fn lean_unbox(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return @intCast(usize, @ptrToInt(o)) >> @intCast(@import("std").math.Log2Int(usize), 1);
}
pub extern fn lean_set_exit_on_panic(flag: bool) void;
pub extern fn lean_set_panic_messages(flag: bool) void;
pub extern fn lean_panic_fn(default_val: ?*lean_object, msg: ?*lean_object) ?*lean_object;
pub extern fn lean_internal_panic(msg: [*c]const u8) noreturn;
pub extern fn lean_internal_panic_out_of_memory(...) noreturn;
pub extern fn lean_internal_panic_unreachable(...) noreturn;
pub extern fn lean_internal_panic_rc_overflow(...) noreturn;
pub fn lean_align(arg_v: usize, arg_a: usize) callconv(.C) usize {
    var v = arg_v;
    var a = arg_a;
    return ((v / a) *% a) +% (a *% @bitCast(c_ulong, @as(c_long, @boolToInt((v % a) != @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))))));
}
pub fn lean_get_slot_idx(arg_sz: c_uint) callconv(.C) c_uint {
    var sz = arg_sz;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz > @bitCast(c_uint, @as(c_int, 0)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 308), "sz > 0");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(lean_align(@bitCast(usize, @as(c_ulong, sz)), @bitCast(usize, @as(c_long, @as(c_int, 8)))) == @bitCast(c_ulong, @as(c_ulong, sz)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 309), "lean_align(sz, LEAN_OBJECT_SIZE_DELTA) == sz");
        }
    }
    return (sz / @bitCast(c_uint, @as(c_int, 8))) -% @bitCast(c_uint, @as(c_int, 1));
}
pub extern fn lean_alloc_small(sz: c_uint, slot_idx: c_uint) ?*anyopaque;
pub extern fn lean_free_small(p: ?*anyopaque) void;
pub extern fn lean_small_mem_size(p: ?*anyopaque) c_uint;
pub extern fn lean_inc_heartbeat(...) void;
pub extern fn malloc(c_ulong) ?*anyopaque;
pub fn lean_alloc_small_object(arg_sz: c_uint) callconv(.C) ?*lean_object {
    var sz = arg_sz;
    sz = @bitCast(c_uint, @truncate(c_uint, lean_align(@bitCast(usize, @as(c_ulong, sz)), @bitCast(usize, @as(c_long, @as(c_int, 8))))));
    var slot_idx: c_uint = lean_get_slot_idx(sz);
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz <= @bitCast(c_uint, @as(c_int, 4096)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 326), "sz <= LEAN_MAX_SMALL_OBJECT_SIZE");
        }
    }
    return @ptrCast(?*lean_object, lean_alloc_small(sz, slot_idx));
}
pub fn lean_alloc_ctor_memory(arg_sz: c_uint) callconv(.C) ?*lean_object {
    var sz = arg_sz;
    var sz1: c_uint = @bitCast(c_uint, @truncate(c_uint, lean_align(@bitCast(usize, @as(c_ulong, sz)), @bitCast(usize, @as(c_long, @as(c_int, 8))))));
    var slot_idx: c_uint = lean_get_slot_idx(sz1);
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz1 <= @bitCast(c_uint, @as(c_int, 4096)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 341), "sz1 <= LEAN_MAX_SMALL_OBJECT_SIZE");
        }
    }
    var r: ?*lean_object = @ptrCast(?*lean_object, lean_alloc_small(sz1, slot_idx));
    if (sz1 > sz) {
        var end: [*c]usize = @ptrCast([*c]usize, @alignCast(@import("std").meta.alignment([*c]usize), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), r)) + sz1));
        (blk: {
            const tmp = -@as(c_int, 1);
            if (tmp >= 0) break :blk end + @intCast(usize, tmp) else break :blk end - ~@bitCast(usize, @intCast(isize, tmp) +% -1);
        }).* = 0;
    }
    return r;
}
pub fn lean_small_object_size(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    return lean_small_mem_size(@ptrCast(?*anyopaque, o));
}
pub extern fn free(?*anyopaque) void;
pub fn lean_free_small_object(arg_o: ?*lean_object) callconv(.C) void {
    var o = arg_o;
    lean_free_small(@ptrCast(?*anyopaque, o));
}
pub extern fn lean_alloc_object(sz: usize) ?*lean_object;
pub extern fn lean_free_object(o: ?*lean_object) void;
pub fn lean_ptr_tag(arg_o: ?*lean_object) callconv(.C) u8 {
    var o = arg_o;
    return @bitCast(u8, @truncate(u8, o.*.m_tag));
}
pub fn lean_ptr_other(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    return o.*.m_other;
}
pub extern fn lean_object_byte_size(o: ?*lean_object) usize;
pub fn lean_is_mt(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return o.*.m_rc < @as(c_int, 0);
}
pub fn lean_is_st(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return o.*.m_rc > @as(c_int, 0);
}
pub fn lean_is_persistent(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return o.*.m_rc == @as(c_int, 0);
}
pub fn lean_has_rc(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return o.*.m_rc != @as(c_int, 0);
} // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:417:30: warning: unsupported type: 'Atomic'
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:417:30: warning: unsupported function proto return type
pub const lean_get_rc_mt_addr = @compileError("unable to resolve prototype of function"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:417:30
pub extern fn lean_inc_ref_cold(o: ?*lean_object) void;
pub extern fn lean_inc_ref_n_cold(o: ?*lean_object, n: c_uint) void;
pub fn lean_inc_ref(arg_o: ?*lean_object) callconv(.C) void {
    var o = arg_o;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_st(o))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        o.*.m_rc += 1;
    } else if (o.*.m_rc != @as(c_int, 0)) {
        lean_inc_ref_cold(o);
    }
}
pub fn lean_inc_ref_n(arg_o: ?*lean_object, arg_n: usize) callconv(.C) void {
    var o = arg_o;
    var n = arg_n;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_st(o))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        o.*.m_rc += @bitCast(c_int, @truncate(c_uint, n));
    } else if (o.*.m_rc != @as(c_int, 0)) {
        lean_inc_ref_n_cold(o, @bitCast(c_uint, @truncate(c_uint, n)));
    }
}
pub extern fn lean_dec_ref_cold(o: ?*lean_object) void;
pub fn lean_dec_ref(arg_o: ?*lean_object) callconv(.C) void {
    var o = arg_o;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(o.*.m_rc > @as(c_int, 1)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        o.*.m_rc -= 1;
    } else if (o.*.m_rc != @as(c_int, 0)) {
        lean_dec_ref_cold(o);
    }
}
pub fn lean_inc(arg_o: ?*lean_object) callconv(.C) void {
    var o = arg_o;
    if (!lean_is_scalar(o)) {
        lean_inc_ref(o);
    }
}
pub fn lean_inc_n(arg_o: ?*lean_object, arg_n: usize) callconv(.C) void {
    var o = arg_o;
    var n = arg_n;
    if (!lean_is_scalar(o)) {
        lean_inc_ref_n(o, n);
    }
}
pub fn lean_dec(arg_o: ?*lean_object) callconv(.C) void {
    var o = arg_o;
    if (!lean_is_scalar(o)) {
        lean_dec_ref(o);
    }
}
pub extern fn lean_dealloc(o: ?*lean_object) void;
pub fn lean_is_ctor(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) <= @as(c_int, 244);
}
pub fn lean_is_closure(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 245);
}
pub fn lean_is_array(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 246);
}
pub fn lean_is_sarray(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 248);
}
pub fn lean_is_string(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 249);
}
pub fn lean_is_mpz(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 250);
}
pub fn lean_is_thunk(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 251);
}
pub fn lean_is_task(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 252);
}
pub fn lean_is_external(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 254);
}
pub fn lean_is_ref(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(o))) == @as(c_int, 253);
}
pub fn lean_obj_tag(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    if (lean_is_scalar(o)) return @bitCast(c_uint, @truncate(c_uint, lean_unbox(o))) else return @bitCast(c_uint, @as(c_uint, lean_ptr_tag(o)));
    return 0;
}
pub fn lean_to_ctor(arg_o: ?*lean_object) callconv(.C) ?*lean_ctor_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_ctor(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 471), "lean_is_ctor(o)");
        }
    }
    return @ptrCast(?*lean_ctor_object, @alignCast(@import("std").meta.alignment(?*lean_ctor_object), o));
}
pub fn lean_to_closure(arg_o: ?*lean_object) callconv(.C) ?*lean_closure_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_closure(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 472), "lean_is_closure(o)");
        }
    }
    return @ptrCast(?*lean_closure_object, @alignCast(@import("std").meta.alignment(?*lean_closure_object), o));
}
pub fn lean_to_array(arg_o: ?*lean_object) callconv(.C) ?*lean_array_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_array(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 473), "lean_is_array(o)");
        }
    }
    return @ptrCast(?*lean_array_object, @alignCast(@import("std").meta.alignment(?*lean_array_object), o));
}
pub fn lean_to_sarray(arg_o: ?*lean_object) callconv(.C) ?*lean_sarray_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_sarray(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 474), "lean_is_sarray(o)");
        }
    }
    return @ptrCast(?*lean_sarray_object, @alignCast(@import("std").meta.alignment(?*lean_sarray_object), o));
}
pub fn lean_to_string(arg_o: ?*lean_object) callconv(.C) ?*lean_string_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_string(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 475), "lean_is_string(o)");
        }
    }
    return @ptrCast(?*lean_string_object, @alignCast(@import("std").meta.alignment(?*lean_string_object), o));
}
pub fn lean_to_thunk(arg_o: ?*lean_object) callconv(.C) ?*lean_thunk_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_thunk(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 476), "lean_is_thunk(o)");
        }
    }
    return @ptrCast(?*lean_thunk_object, @alignCast(@import("std").meta.alignment(?*lean_thunk_object), o));
}
pub fn lean_to_task(arg_o: ?*lean_object) callconv(.C) ?*lean_task_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_task(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 477), "lean_is_task(o)");
        }
    }
    return @ptrCast(?*lean_task_object, @alignCast(@import("std").meta.alignment(?*lean_task_object), o));
}
pub fn lean_to_ref(arg_o: ?*lean_object) callconv(.C) ?*lean_ref_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_ref(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 478), "lean_is_ref(o)");
        }
    }
    return @ptrCast(?*lean_ref_object, @alignCast(@import("std").meta.alignment(?*lean_ref_object), o));
}
pub fn lean_to_external(arg_o: ?*lean_object) callconv(.C) ?*lean_external_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_external(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 479), "lean_is_external(o)");
        }
    }
    return @ptrCast(?*lean_external_object, @alignCast(@import("std").meta.alignment(?*lean_external_object), o));
}
pub fn lean_is_exclusive(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_st(o))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return o.*.m_rc == @as(c_int, 1);
    } else {
        return @as(c_int, 0) != 0;
    }
    return false;
}
pub fn lean_is_shared(arg_o: ?*lean_object) callconv(.C) bool {
    var o = arg_o;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_st(o))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return o.*.m_rc > @as(c_int, 1);
    } else {
        return @as(c_int, 0) != 0;
    }
    return false;
}
pub extern fn lean_mark_mt(o: ?*lean_object) void;
pub extern fn lean_mark_persistent(o: ?*lean_object) void;
pub fn lean_set_st_header(arg_o: ?*lean_object, arg_tag: c_uint, arg_other: c_uint) callconv(.C) void {
    var o = arg_o;
    var tag = arg_tag;
    var other = arg_other;
    o.*.m_rc = 1;
    o.*.m_tag = tag;
    o.*.m_other = other;
    o.*.m_cs_sz = 0;
}
pub fn lean_set_non_heap_header(arg_o: ?*lean_object, arg_sz: usize, arg_tag: c_uint, arg_other: c_uint) callconv(.C) void {
    var o = arg_o;
    var sz = arg_sz;
    var tag = arg_tag;
    var other = arg_other;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz > @bitCast(c_ulong, @as(c_long, @as(c_int, 0))))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 510), "sz > 0");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulonglong, @as(c_ulonglong, sz)) < (@as(c_ulonglong, 1) << @intCast(@import("std").math.Log2Int(c_ulonglong), 16)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 511), "sz < (1ull << 16)");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!((sz == @bitCast(c_ulong, @as(c_long, @as(c_int, 1)))) or !lean_is_big_object_tag(@bitCast(u8, @truncate(u8, tag))))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 512), "sz == 1 || !lean_is_big_object_tag(tag)");
        }
    }
    o.*.m_rc = 0;
    o.*.m_tag = tag;
    o.*.m_other = other;
    o.*.m_cs_sz = @bitCast(c_uint, @truncate(c_uint, sz));
}
pub fn lean_set_non_heap_header_for_big(arg_o: ?*lean_object, arg_tag: c_uint, arg_other: c_uint) callconv(.C) void {
    var o = arg_o;
    var tag = arg_tag;
    var other = arg_other;
    lean_set_non_heap_header(o, @bitCast(usize, @as(c_long, @as(c_int, 1))), tag, other);
}
pub fn lean_ctor_num_objs(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_ctor(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 527), "lean_is_ctor(o)");
        }
    }
    return lean_ptr_other(o);
}
pub fn lean_ctor_obj_cptr(arg_o: ?*lean_object) callconv(.C) [*c]?*lean_object {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_ctor(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 532), "lean_is_ctor(o)");
        }
    }
    return lean_to_ctor(o).*.m_objs();
}
pub fn lean_ctor_scalar_cptr(arg_o: ?*lean_object) callconv(.C) [*c]u8 {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_ctor(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 537), "lean_is_ctor(o)");
        }
    }
    return @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o) + lean_ctor_num_objs(o)));
}
pub fn lean_alloc_ctor(arg_tag: c_uint, arg_num_objs: c_uint, arg_scalar_sz: c_uint) callconv(.C) ?*lean_object {
    var tag = arg_tag;
    var num_objs = arg_num_objs;
    var scalar_sz = arg_scalar_sz;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(((tag <= @bitCast(c_uint, @as(c_int, 244))) and (num_objs < @bitCast(c_uint, @as(c_int, 256)))) and (scalar_sz < @bitCast(c_uint, @as(c_int, 1024))))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 542), "tag <= LeanMaxCtorTag && num_objs < LEAN_MAX_CTOR_FIELDS && scalar_sz < LEAN_MAX_CTOR_SCALARS_SIZE");
        }
    }
    var o: ?*lean_object = lean_alloc_ctor_memory(@bitCast(c_uint, @truncate(c_uint, (@sizeOf(lean_ctor_object) +% (@sizeOf(?*anyopaque) *% @bitCast(c_ulong, @as(c_ulong, num_objs)))) +% @bitCast(c_ulong, @as(c_ulong, scalar_sz)))));
    lean_set_st_header(o, tag, num_objs);
    return o;
}
pub fn lean_ctor_get(arg_o: b_lean_obj_arg, arg_i: c_uint) callconv(.C) b_lean_obj_res {
    var o = arg_o;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_ctor_num_objs(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 549), "i < lean_ctor_num_objs(o)");
        }
    }
    return lean_ctor_obj_cptr(o)[i];
}
pub fn lean_ctor_set(arg_o: b_lean_obj_arg, arg_i: c_uint, arg_v: lean_obj_arg) callconv(.C) void {
    var o = arg_o;
    var i = arg_i;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_ctor_num_objs(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 554), "i < lean_ctor_num_objs(o)");
        }
    }
    lean_ctor_obj_cptr(o)[i] = v;
}
pub fn lean_ctor_set_tag(arg_o: b_lean_obj_arg, arg_new_tag: u8) callconv(.C) void {
    var o = arg_o;
    var new_tag = arg_new_tag;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_int, @as(c_uint, new_tag)) <= @as(c_int, 244))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 559), "new_tag <= LeanMaxCtorTag");
        }
    }
    o.*.m_tag = @bitCast(c_uint, @as(c_uint, new_tag));
}
pub fn lean_ctor_release(arg_o: b_lean_obj_arg, arg_i: c_uint) callconv(.C) void {
    var o = arg_o;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_ctor_num_objs(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 564), "i < lean_ctor_num_objs(o)");
        }
    }
    var objs: [*c]?*lean_object = lean_ctor_obj_cptr(o);
    lean_dec(objs[i]);
    objs[i] = lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0))));
}
pub fn lean_ctor_get_usize(arg_o: b_lean_obj_arg, arg_i: c_uint) callconv(.C) usize {
    var o = arg_o;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i >= lean_ctor_num_objs(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 571), "i >= lean_ctor_num_objs(o)");
        }
    }
    return @ptrCast([*c]usize, @alignCast(@import("std").meta.alignment([*c]usize), lean_ctor_obj_cptr(o) + i)).*;
}
pub fn lean_ctor_get_uint8(arg_o: b_lean_obj_arg, arg_offset: c_uint) callconv(.C) u8 {
    var o = arg_o;
    var offset = arg_offset;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 576), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    return (@ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset).*;
}
pub fn lean_ctor_get_uint16(arg_o: b_lean_obj_arg, arg_offset: c_uint) callconv(.C) u16 {
    var o = arg_o;
    var offset = arg_offset;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 581), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    return @ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).*;
}
pub fn lean_ctor_get_uint32(arg_o: b_lean_obj_arg, arg_offset: c_uint) callconv(.C) u32 {
    var o = arg_o;
    var offset = arg_offset;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 586), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    return @ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).*;
}
pub fn lean_ctor_get_uint64(arg_o: b_lean_obj_arg, arg_offset: c_uint) callconv(.C) u64 {
    var o = arg_o;
    var offset = arg_offset;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 591), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    return @ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).*;
}
pub fn lean_ctor_get_float(arg_o: b_lean_obj_arg, arg_offset: c_uint) callconv(.C) f64 {
    var o = arg_o;
    var offset = arg_offset;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 596), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    return @ptrCast([*c]f64, @alignCast(@import("std").meta.alignment([*c]f64), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).*;
}
pub fn lean_ctor_set_usize(arg_o: b_lean_obj_arg, arg_i: c_uint, arg_v: usize) callconv(.C) void {
    var o = arg_o;
    var i = arg_i;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i >= lean_ctor_num_objs(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 601), "i >= lean_ctor_num_objs(o)");
        }
    }
    @ptrCast([*c]usize, @alignCast(@import("std").meta.alignment([*c]usize), lean_ctor_obj_cptr(o) + i)).* = v;
}
pub fn lean_ctor_set_uint8(arg_o: b_lean_obj_arg, arg_offset: c_uint, arg_v: u8) callconv(.C) void {
    var o = arg_o;
    var offset = arg_offset;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 606), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    (@ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset).* = v;
}
pub fn lean_ctor_set_uint16(arg_o: b_lean_obj_arg, arg_offset: c_uint, arg_v: u16) callconv(.C) void {
    var o = arg_o;
    var offset = arg_offset;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 611), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    @ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).* = v;
}
pub fn lean_ctor_set_uint32(arg_o: b_lean_obj_arg, arg_offset: c_uint, arg_v: u32) callconv(.C) void {
    var o = arg_o;
    var offset = arg_offset;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 616), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    @ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).* = v;
}
pub fn lean_ctor_set_uint64(arg_o: b_lean_obj_arg, arg_offset: c_uint, arg_v: u64) callconv(.C) void {
    var o = arg_o;
    var offset = arg_offset;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 621), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    @ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).* = v;
}
pub fn lean_ctor_set_float(arg_o: b_lean_obj_arg, arg_offset: c_uint, arg_v: f64) callconv(.C) void {
    var o = arg_o;
    var offset = arg_offset;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(@bitCast(c_ulong, @as(c_ulong, offset)) >= (@bitCast(c_ulong, @as(c_ulong, lean_ctor_num_objs(o))) *% @sizeOf(?*anyopaque)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 626), "offset >= lean_ctor_num_objs(o) * sizeof(void*)");
        }
    }
    @ptrCast([*c]f64, @alignCast(@import("std").meta.alignment([*c]f64), @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), lean_ctor_obj_cptr(o))) + offset)).* = v;
}
pub fn lean_closure_fun(arg_o: ?*lean_object) callconv(.C) ?*anyopaque {
    var o = arg_o;
    return lean_to_closure(o).*.m_fun;
}
pub fn lean_closure_arity(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    return @bitCast(c_uint, @as(c_uint, lean_to_closure(o).*.m_arity));
}
pub fn lean_closure_num_fixed(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    return @bitCast(c_uint, @as(c_uint, lean_to_closure(o).*.m_num_fixed));
}
pub fn lean_closure_arg_cptr(arg_o: ?*lean_object) callconv(.C) [*c]?*lean_object {
    var o = arg_o;
    return lean_to_closure(o).*.m_objs();
}
pub fn lean_alloc_closure(arg_fun: ?*anyopaque, arg_arity: c_uint, arg_num_fixed: c_uint) callconv(.C) lean_obj_res {
    var fun = arg_fun;
    var arity = arg_arity;
    var num_fixed = arg_num_fixed;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(arity > @bitCast(c_uint, @as(c_int, 0)))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 637), "arity > 0");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(num_fixed < arity)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 638), "num_fixed < arity");
        }
    }
    var o: ?*lean_closure_object = @ptrCast(?*lean_closure_object, @alignCast(@import("std").meta.alignment(?*lean_closure_object), lean_alloc_small_object(@bitCast(c_uint, @truncate(c_uint, @sizeOf(lean_closure_object) +% (@sizeOf(?*anyopaque) *% @bitCast(c_ulong, @as(c_ulong, num_fixed))))))));
    lean_set_st_header(@ptrCast(?*lean_object, o), @bitCast(c_uint, @as(c_int, 245)), @bitCast(c_uint, @as(c_int, 0)));
    o.*.m_fun = fun;
    o.*.m_arity = @bitCast(u16, @truncate(c_ushort, arity));
    o.*.m_num_fixed = @bitCast(u16, @truncate(c_ushort, num_fixed));
    return @ptrCast(?*lean_object, o);
}
pub fn lean_closure_get(arg_o: b_lean_obj_arg, arg_i: c_uint) callconv(.C) b_lean_obj_res {
    var o = arg_o;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_closure_num_fixed(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 647), "i < lean_closure_num_fixed(o)");
        }
    }
    return lean_to_closure(o).*.m_objs()[i];
}
pub fn lean_closure_set(arg_o: u_lean_obj_arg, arg_i: c_uint, arg_a: lean_obj_arg) callconv(.C) void {
    var o = arg_o;
    var i = arg_i;
    var a = arg_a;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_closure_num_fixed(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 651), "i < lean_closure_num_fixed(o)");
        }
    }
    lean_to_closure(o).*.m_objs()[i] = a;
}
pub extern fn lean_apply_1(f: ?*lean_object, a1: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_2(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_3(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_4(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_5(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_6(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_7(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_8(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_9(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_10(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_11(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_12(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object, a12: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_13(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object, a12: ?*lean_object, a13: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_14(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object, a12: ?*lean_object, a13: ?*lean_object, a14: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_15(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object, a12: ?*lean_object, a13: ?*lean_object, a14: ?*lean_object, a15: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_16(f: ?*lean_object, a1: ?*lean_object, a2: ?*lean_object, a3: ?*lean_object, a4: ?*lean_object, a5: ?*lean_object, a6: ?*lean_object, a7: ?*lean_object, a8: ?*lean_object, a9: ?*lean_object, a10: ?*lean_object, a11: ?*lean_object, a12: ?*lean_object, a13: ?*lean_object, a14: ?*lean_object, a15: ?*lean_object, a16: ?*lean_object) ?*lean_object;
pub extern fn lean_apply_n(f: ?*lean_object, n: c_uint, args: [*c]?*lean_object) ?*lean_object;
pub extern fn lean_apply_m(f: ?*lean_object, n: c_uint, args: [*c]?*lean_object) ?*lean_object;
pub fn lean_alloc_array(arg_size: usize, arg_capacity: usize) callconv(.C) lean_obj_res {
    var size = arg_size;
    var capacity = arg_capacity;
    var o: ?*lean_array_object = @ptrCast(?*lean_array_object, @alignCast(@import("std").meta.alignment(?*lean_array_object), lean_alloc_object(@sizeOf(lean_array_object) +% (@sizeOf(?*anyopaque) *% capacity))));
    lean_set_st_header(@ptrCast(?*lean_object, o), @bitCast(c_uint, @as(c_int, 246)), @bitCast(c_uint, @as(c_int, 0)));
    o.*.m_size = size;
    o.*.m_capacity = capacity;
    return @ptrCast(?*lean_object, o);
}
pub fn lean_array_size(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_to_array(o).*.m_size;
}
pub fn lean_array_capacity(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_to_array(o).*.m_capacity;
}
pub fn lean_array_byte_size(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return @sizeOf(lean_array_object) +% (@sizeOf(?*anyopaque) *% lean_array_capacity(o));
}
pub fn lean_array_cptr(arg_o: ?*lean_object) callconv(.C) [*c]?*lean_object {
    var o = arg_o;
    return lean_to_array(o).*.m_data();
}
pub fn lean_array_set_size(arg_o: u_lean_obj_arg, arg_sz: usize) callconv(.C) void {
    var o = arg_o;
    var sz = arg_sz;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_array(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 690), "lean_is_array(o)");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_exclusive(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 691), "lean_is_exclusive(o)");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz <= lean_array_capacity(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 692), "sz <= lean_array_capacity(o)");
        }
    }
    lean_to_array(o).*.m_size = sz;
}
pub fn lean_array_get_core(arg_o: b_lean_obj_arg, arg_i: usize) callconv(.C) b_lean_obj_res {
    var o = arg_o;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_array_size(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 696), "i < lean_array_size(o)");
        }
    }
    return lean_to_array(o).*.m_data()[i];
}
pub fn lean_array_set_core(arg_o: u_lean_obj_arg, arg_i: usize, arg_v: lean_obj_arg) callconv(.C) void {
    var o = arg_o;
    var i = arg_i;
    var v = arg_v;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(!lean_has_rc(o) or (@as(c_int, @boolToInt(lean_is_exclusive(o))) != 0))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 702), "!lean_has_rc(o) || lean_is_exclusive(o)");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_array_size(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 703), "i < lean_array_size(o)");
        }
    }
    lean_to_array(o).*.m_data()[i] = v;
}
pub extern fn lean_array_mk(l: lean_obj_arg) ?*lean_object;
pub extern fn lean_array_data(a: lean_obj_arg) ?*lean_object;
pub fn lean_array_sz(arg_a: lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var r: ?*lean_object = lean_box(lean_array_size(a));
    lean_dec(a);
    return r;
}
pub fn lean_array_get_size(arg_a: b_lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    return lean_box(lean_array_size(a));
}
pub fn lean_mk_empty_array() callconv(.C) ?*lean_object {
    return lean_alloc_array(@bitCast(usize, @as(c_long, @as(c_int, 0))), @bitCast(usize, @as(c_long, @as(c_int, 0))));
}
pub fn lean_mk_empty_array_with_capacity(arg_capacity: b_lean_obj_arg) callconv(.C) ?*lean_object {
    var capacity = arg_capacity;
    if (!lean_is_scalar(capacity)) {
        lean_internal_panic_out_of_memory();
    }
    return lean_alloc_array(@bitCast(usize, @as(c_long, @as(c_int, 0))), lean_unbox(capacity));
}
pub fn lean_array_uget(arg_a: b_lean_obj_arg, arg_i: usize) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var r: ?*lean_object = lean_array_get_core(a, i);
    lean_inc(r);
    return r;
}
pub fn lean_array_fget(arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    return lean_array_uget(a, lean_unbox(i));
}
pub extern fn lean_array_get_panic(def_val: lean_obj_arg) lean_obj_res;
pub fn lean_array_get(arg_def_val: lean_obj_arg, arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) ?*lean_object {
    var def_val = arg_def_val;
    var a = arg_a;
    var i = arg_i;
    if (lean_is_scalar(i)) {
        var idx: usize = lean_unbox(i);
        if (idx < lean_array_size(a)) {
            lean_dec(def_val);
            return lean_array_uget(a, idx);
        }
    }
    return lean_array_get_panic(def_val);
}
pub extern fn lean_copy_expand_array(a: lean_obj_arg, expand: bool) lean_obj_res;
pub fn lean_copy_array(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_copy_expand_array(a, @as(c_int, 0) != 0);
}
pub fn lean_ensure_exclusive_array(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    if (lean_is_exclusive(a)) return a;
    return lean_copy_array(a);
}
pub fn lean_array_uset(arg_a: lean_obj_arg, arg_i: usize, arg_v: lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var v = arg_v;
    var r: ?*lean_object = lean_ensure_exclusive_array(a);
    var it: [*c]?*lean_object = lean_array_cptr(r) + i;
    lean_dec(it.*);
    it.* = v;
    return r;
}
pub fn lean_array_fset(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_v: lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var v = arg_v;
    return lean_array_uset(a, lean_unbox(i), v);
}
pub extern fn lean_array_set_panic(a: lean_obj_arg, v: lean_obj_arg) lean_obj_res;
pub fn lean_array_set(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_v: lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var v = arg_v;
    if (lean_is_scalar(i)) {
        var idx: usize = lean_unbox(i);
        if (idx < lean_array_size(a)) return lean_array_uset(a, idx, v);
    }
    return lean_array_set_panic(a, v);
}
pub fn lean_array_pop(arg_a: lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var r: ?*lean_object = lean_ensure_exclusive_array(a);
    var sz: usize = lean_to_array(r).*.m_size;
    var last: [*c]?*lean_object = undefined;
    if (sz == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) return r;
    sz -%= 1;
    last = lean_array_cptr(r) + sz;
    lean_to_array(r).*.m_size = sz;
    lean_dec(last.*);
    return r;
}
pub fn lean_array_uswap(arg_a: lean_obj_arg, arg_i: usize, arg_j: usize) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var j = arg_j;
    var r: ?*lean_object = lean_ensure_exclusive_array(a);
    var it: [*c]?*lean_object = lean_array_cptr(r);
    var v1: ?*lean_object = it[i];
    it[i] = it[j];
    it[j] = v1;
    return r;
}
pub fn lean_array_fswap(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_j: b_lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var j = arg_j;
    return lean_array_uswap(a, lean_unbox(i), lean_unbox(j));
}
pub fn lean_array_swap(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_j: b_lean_obj_arg) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var j = arg_j;
    if (!lean_is_scalar(i) or !lean_is_scalar(j)) return a;
    var ui: usize = lean_unbox(i);
    var uj: usize = lean_unbox(j);
    var sz: usize = lean_to_array(a).*.m_size;
    if ((ui >= sz) or (uj >= sz)) return a;
    return lean_array_uswap(a, ui, uj);
}
pub extern fn lean_array_push(a: lean_obj_arg, v: lean_obj_arg) ?*lean_object;
pub extern fn lean_mk_array(n: lean_obj_arg, v: lean_obj_arg) ?*lean_object;
pub fn lean_alloc_sarray(arg_elem_size: c_uint, arg_size: usize, arg_capacity: usize) callconv(.C) lean_obj_res {
    var elem_size = arg_elem_size;
    var size = arg_size;
    var capacity = arg_capacity;
    var o: ?*lean_sarray_object = @ptrCast(?*lean_sarray_object, @alignCast(@import("std").meta.alignment(?*lean_sarray_object), lean_alloc_object(@sizeOf(lean_sarray_object) +% (@bitCast(c_ulong, @as(c_ulong, elem_size)) *% capacity))));
    lean_set_st_header(@ptrCast(?*lean_object, o), @bitCast(c_uint, @as(c_int, 248)), elem_size);
    o.*.m_size = size;
    o.*.m_capacity = capacity;
    return @ptrCast(?*lean_object, o);
}
pub fn lean_sarray_elem_size(arg_o: ?*lean_object) callconv(.C) c_uint {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_sarray(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 837), "lean_is_sarray(o)");
        }
    }
    return lean_ptr_other(o);
}
pub fn lean_sarray_capacity(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return lean_to_sarray(o).*.m_capacity;
}
pub fn lean_sarray_byte_size(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return @sizeOf(lean_sarray_object) +% (@bitCast(c_ulong, @as(c_ulong, lean_sarray_elem_size(o))) *% lean_sarray_capacity(o));
}
pub fn lean_sarray_size(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_to_sarray(o).*.m_size;
}
pub fn lean_sarray_set_size(arg_o: u_lean_obj_arg, arg_sz: usize) callconv(.C) void {
    var o = arg_o;
    var sz = arg_sz;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_exclusive(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 846), "lean_is_exclusive(o)");
        }
    }
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(sz <= lean_sarray_capacity(o))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 847), "sz <= lean_sarray_capacity(o)");
        }
    }
    lean_to_sarray(o).*.m_size = sz;
}
pub fn lean_sarray_cptr(arg_o: ?*lean_object) callconv(.C) [*c]u8 {
    var o = arg_o;
    return lean_to_sarray(o).*.m_data();
}
pub extern fn lean_byte_array_mk(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_byte_array_data(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_copy_byte_array(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_byte_array_hash(a: b_lean_obj_arg) u64;
pub fn lean_mk_empty_byte_array(arg_capacity: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var capacity = arg_capacity;
    if (!lean_is_scalar(capacity)) {
        lean_internal_panic_out_of_memory();
    }
    return lean_alloc_sarray(@bitCast(c_uint, @as(c_int, 1)), @bitCast(usize, @as(c_long, @as(c_int, 0))), lean_unbox(capacity));
}
pub fn lean_byte_array_size(arg_a: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_box(lean_sarray_size(a));
}
pub fn lean_byte_array_uget(arg_a: b_lean_obj_arg, arg_i: usize) callconv(.C) u8 {
    var a = arg_a;
    var i = arg_i;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!(i < lean_sarray_size(a))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 870), "i < lean_sarray_size(a)");
        }
    }
    return lean_sarray_cptr(a)[i];
}
pub fn lean_byte_array_get(arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) u8 {
    var a = arg_a;
    var i = arg_i;
    if (lean_is_scalar(i)) {
        var idx: usize = lean_unbox(i);
        return @bitCast(u8, @truncate(i8, if (idx < lean_sarray_size(a)) @bitCast(c_int, @as(c_uint, lean_byte_array_uget(a, idx))) else @as(c_int, 0)));
    } else {
        return 0;
    }
    return 0;
}
pub fn lean_byte_array_fget(arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) u8 {
    var a = arg_a;
    var i = arg_i;
    return lean_byte_array_uget(a, lean_unbox(i));
}
pub extern fn lean_byte_array_push(a: lean_obj_arg, b: u8) lean_obj_res;
pub fn lean_byte_array_uset(arg_a: lean_obj_arg, arg_i: usize, arg_v: u8) callconv(.C) ?*lean_object {
    var a = arg_a;
    var i = arg_i;
    var v = arg_v;
    var r: lean_obj_res = undefined;
    if (lean_is_exclusive(a)) {
        r = a;
    } else {
        r = lean_copy_byte_array(a);
    }
    var it: [*c]u8 = lean_sarray_cptr(r) + i;
    it.* = v;
    return r;
}
pub fn lean_byte_array_set(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_b: u8) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    var b = arg_b;
    if (!lean_is_scalar(i)) {
        return a;
    } else {
        var idx: usize = lean_unbox(i);
        if (idx >= lean_sarray_size(a)) {
            return a;
        } else {
            return lean_byte_array_uset(a, idx, b);
        }
    }
    return null;
}
pub fn lean_byte_array_fset(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_b: u8) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    var b = arg_b;
    return lean_byte_array_uset(a, lean_unbox(i), b);
}
pub extern fn lean_float_array_mk(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_float_array_data(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_copy_float_array(a: lean_obj_arg) lean_obj_res;
pub fn lean_mk_empty_float_array(arg_capacity: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var capacity = arg_capacity;
    if (!lean_is_scalar(capacity)) {
        lean_internal_panic_out_of_memory();
    }
    return lean_alloc_sarray(@bitCast(c_uint, @truncate(c_uint, @sizeOf(f64))), @bitCast(usize, @as(c_long, @as(c_int, 0))), lean_unbox(capacity));
}
pub fn lean_float_array_size(arg_a: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_box(lean_sarray_size(a));
}
pub fn lean_float_array_cptr(arg_a: b_lean_obj_arg) callconv(.C) [*c]f64 {
    var a = arg_a;
    return @ptrCast([*c]f64, @alignCast(@import("std").meta.alignment([*c]f64), lean_sarray_cptr(a)));
}
pub fn lean_float_array_uget(arg_a: b_lean_obj_arg, arg_i: usize) callconv(.C) f64 {
    var a = arg_a;
    var i = arg_i;
    return lean_float_array_cptr(a)[i];
}
pub fn lean_float_array_fget(arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) f64 {
    var a = arg_a;
    var i = arg_i;
    return lean_float_array_uget(a, lean_unbox(i));
}
pub fn lean_float_array_get(arg_a: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) f64 {
    var a = arg_a;
    var i = arg_i;
    if (lean_is_scalar(i)) {
        var idx: usize = lean_unbox(i);
        return if (idx < lean_sarray_size(a)) lean_float_array_uget(a, idx) else 0.0;
    } else {
        return 0.0;
    }
    return 0;
}
pub extern fn lean_float_array_push(a: lean_obj_arg, d: f64) lean_obj_res;
pub fn lean_float_array_uset(arg_a: lean_obj_arg, arg_i: usize, arg_d: f64) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    var d = arg_d;
    var r: lean_obj_res = undefined;
    if (lean_is_exclusive(a)) {
        r = a;
    } else {
        r = lean_copy_float_array(a);
    }
    var it: [*c]f64 = lean_float_array_cptr(r) + i;
    it.* = d;
    return r;
}
pub fn lean_float_array_fset(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_d: f64) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    var d = arg_d;
    return lean_float_array_uset(a, lean_unbox(i), d);
}
pub fn lean_float_array_set(arg_a: lean_obj_arg, arg_i: b_lean_obj_arg, arg_d: f64) callconv(.C) lean_obj_res {
    var a = arg_a;
    var i = arg_i;
    var d = arg_d;
    if (!lean_is_scalar(i)) {
        return a;
    } else {
        var idx: usize = lean_unbox(i);
        if (idx >= lean_sarray_size(a)) {
            return a;
        } else {
            return lean_float_array_uset(a, idx, d);
        }
    }
    return null;
}
pub fn lean_alloc_string(arg_size: usize, arg_capacity: usize, arg_len: usize) callconv(.C) lean_obj_res {
    var size = arg_size;
    var capacity = arg_capacity;
    var len = arg_len;
    var o: ?*lean_string_object = @ptrCast(?*lean_string_object, @alignCast(@import("std").meta.alignment(?*lean_string_object), lean_alloc_object(@sizeOf(lean_string_object) +% capacity)));
    lean_set_st_header(@ptrCast(?*lean_object, o), @bitCast(c_uint, @as(c_int, 249)), @bitCast(c_uint, @as(c_int, 0)));
    o.*.m_size = size;
    o.*.m_capacity = capacity;
    o.*.m_length = len;
    return @ptrCast(?*lean_object, o);
}
pub extern fn lean_utf8_strlen(str: [*c]const u8) usize;
pub extern fn lean_utf8_n_strlen(str: [*c]const u8, n: usize) usize;
pub fn lean_string_capacity(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return lean_to_string(o).*.m_capacity;
}
pub fn lean_string_byte_size(arg_o: ?*lean_object) callconv(.C) usize {
    var o = arg_o;
    return @sizeOf(lean_string_object) +% lean_string_capacity(o);
}
pub fn lean_char_default_value() callconv(.C) u32 {
    return 'A';
}
pub extern fn lean_mk_string_from_bytes(s: [*c]const u8, sz: usize) lean_obj_res;
pub extern fn lean_mk_string(s: [*c]const u8) lean_obj_res;
pub fn lean_string_cstr(arg_o: b_lean_obj_arg) callconv(.C) [*c]const u8 {
    var o = arg_o;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_string(o)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 998), "lean_is_string(o)");
        }
    }
    return lean_to_string(o).*.m_data();
}
pub fn lean_string_size(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_to_string(o).*.m_size;
}
pub fn lean_string_len(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_to_string(o).*.m_length;
}
pub extern fn lean_string_push(s: lean_obj_arg, c: u32) lean_obj_res;
pub extern fn lean_string_append(s1: lean_obj_arg, s2: b_lean_obj_arg) lean_obj_res;
pub fn lean_string_length(arg_s: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var s = arg_s;
    return lean_box(lean_string_len(s));
}
pub extern fn lean_string_mk(cs: lean_obj_arg) lean_obj_res;
pub extern fn lean_string_data(s: lean_obj_arg) lean_obj_res;
pub extern fn lean_string_utf8_get(s: b_lean_obj_arg, i: b_lean_obj_arg) u32;
pub extern fn lean_string_utf8_get_fast_cold(str: [*c]const u8, i: usize, size: usize, c: u8) u32;
pub fn lean_string_utf8_get_fast(arg_s: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) u32 {
    var s = arg_s;
    var i = arg_i;
    var str: [*c]const u8 = lean_string_cstr(s);
    var idx: usize = lean_unbox(i);
    var c: u8 = @bitCast(u8, str[idx]);
    if ((@bitCast(c_int, @as(c_uint, c)) & @as(c_int, 128)) == @as(c_int, 0)) return @bitCast(u32, @as(c_uint, c));
    return lean_string_utf8_get_fast_cold(str, idx, lean_string_size(s), c);
}
pub extern fn lean_string_utf8_next(s: b_lean_obj_arg, i: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_string_utf8_next_fast_cold(i: usize, c: u8) lean_obj_res;
pub fn lean_string_utf8_next_fast(arg_s: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var s = arg_s;
    var i = arg_i;
    var str: [*c]const u8 = lean_string_cstr(s);
    var idx: usize = lean_unbox(i);
    var c: u8 = @bitCast(u8, str[idx]);
    if ((@bitCast(c_int, @as(c_uint, c)) & @as(c_int, 128)) == @as(c_int, 0)) return lean_box(idx +% @bitCast(c_ulong, @as(c_long, @as(c_int, 1))));
    return lean_string_utf8_next_fast_cold(idx, c);
}
pub extern fn lean_string_utf8_prev(s: b_lean_obj_arg, i: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_string_utf8_set(s: lean_obj_arg, i: b_lean_obj_arg, c: u32) lean_obj_res;
pub fn lean_string_utf8_at_end(arg_s: b_lean_obj_arg, arg_i: b_lean_obj_arg) callconv(.C) u8 {
    var s = arg_s;
    var i = arg_i;
    return @bitCast(u8, @truncate(i8, @boolToInt(!lean_is_scalar(i) or (lean_unbox(i) >= (lean_string_size(s) -% @bitCast(c_ulong, @as(c_long, @as(c_int, 1))))))));
}
pub extern fn lean_string_utf8_extract(s: b_lean_obj_arg, b: b_lean_obj_arg, e: b_lean_obj_arg) lean_obj_res;
pub fn lean_string_utf8_byte_size(arg_s: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var s = arg_s;
    return lean_box(lean_string_size(s) -% @bitCast(c_ulong, @as(c_long, @as(c_int, 1))));
}
pub extern fn lean_string_eq_cold(s1: b_lean_obj_arg, s2: b_lean_obj_arg) bool;
pub fn lean_string_eq(arg_s1: b_lean_obj_arg, arg_s2: b_lean_obj_arg) callconv(.C) bool {
    var s1 = arg_s1;
    var s2 = arg_s2;
    return (s1 == s2) or ((lean_string_size(s1) == lean_string_size(s2)) and (@as(c_int, @boolToInt(lean_string_eq_cold(s1, s2))) != 0));
}
pub fn lean_string_ne(arg_s1: b_lean_obj_arg, arg_s2: b_lean_obj_arg) callconv(.C) bool {
    var s1 = arg_s1;
    var s2 = arg_s2;
    return !lean_string_eq(s1, s2);
}
pub extern fn lean_string_lt(s1: b_lean_obj_arg, s2: b_lean_obj_arg) bool;
pub fn lean_string_dec_eq(arg_s1: b_lean_obj_arg, arg_s2: b_lean_obj_arg) callconv(.C) u8 {
    var s1 = arg_s1;
    var s2 = arg_s2;
    return @as(u8, @boolToInt(lean_string_eq(s1, s2)));
}
pub fn lean_string_dec_lt(arg_s1: b_lean_obj_arg, arg_s2: b_lean_obj_arg) callconv(.C) u8 {
    var s1 = arg_s1;
    var s2 = arg_s2;
    return @as(u8, @boolToInt(lean_string_lt(s1, s2)));
}
pub extern fn lean_string_hash(b_lean_obj_arg) u64; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1050:20: warning: unsupported CastKind NonAtomicToAtomic
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1047:28: warning: unable to translate function, demoted to extern
pub extern fn lean_mk_thunk(arg_c: lean_obj_arg) callconv(.C) lean_obj_res; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1059:20: warning: unsupported CastKind NonAtomicToAtomic
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1056:28: warning: unable to translate function, demoted to extern
pub extern fn lean_thunk_pure(arg_v: lean_obj_arg) callconv(.C) lean_obj_res;
pub extern fn lean_thunk_get_core(t: ?*lean_object) ?*lean_object; // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1067:23: warning: unsupported CastKind AtomicToNonAtomic
// /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:1066:30: warning: unable to translate function, demoted to extern
pub extern fn lean_thunk_get(arg_t: b_lean_obj_arg) callconv(.C) b_lean_obj_res;
pub fn lean_thunk_get_own(arg_t: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var t = arg_t;
    var r: ?*lean_object = lean_thunk_get(t);
    lean_inc(r);
    return r;
}
pub extern fn lean_init_task_manager(...) void;
pub extern fn lean_init_task_manager_using(num_workers: c_uint) void;
pub extern fn lean_finalize_task_manager(...) void;
pub extern fn lean_task_spawn_core(c: lean_obj_arg, prio: c_uint, keep_alive: bool) lean_obj_res;
pub fn lean_task_spawn(arg_c: lean_obj_arg, arg_prio: lean_obj_arg) callconv(.C) lean_obj_res {
    var c = arg_c;
    var prio = arg_prio;
    return lean_task_spawn_core(c, @bitCast(c_uint, @truncate(c_uint, lean_unbox(prio))), @as(c_int, 0) != 0);
}
pub extern fn lean_task_pure(a: lean_obj_arg) lean_obj_res;
pub extern fn lean_task_bind_core(x: lean_obj_arg, f: lean_obj_arg, prio: c_uint, keep_alive: bool) lean_obj_res;
pub fn lean_task_bind(arg_x: lean_obj_arg, arg_f: lean_obj_arg, arg_prio: lean_obj_arg) callconv(.C) lean_obj_res {
    var x = arg_x;
    var f = arg_f;
    var prio = arg_prio;
    return lean_task_bind_core(x, f, @bitCast(c_uint, @truncate(c_uint, lean_unbox(prio))), @as(c_int, 0) != 0);
}
pub extern fn lean_task_map_core(f: lean_obj_arg, t: lean_obj_arg, prio: c_uint, keep_alive: bool) lean_obj_res;
pub fn lean_task_map(arg_f: lean_obj_arg, arg_t: lean_obj_arg, arg_prio: lean_obj_arg) callconv(.C) lean_obj_res {
    var f = arg_f;
    var t = arg_t;
    var prio = arg_prio;
    return lean_task_map_core(f, t, @bitCast(c_uint, @truncate(c_uint, lean_unbox(prio))), @as(c_int, 0) != 0);
}
pub extern fn lean_task_get(t: b_lean_obj_arg) b_lean_obj_res;
pub fn lean_task_get_own(arg_t: lean_obj_arg) callconv(.C) lean_obj_res {
    var t = arg_t;
    var r: ?*lean_object = lean_task_get(t);
    lean_inc(r);
    lean_dec(t);
    return r;
}
pub extern fn lean_io_check_canceled_core(...) bool;
pub extern fn lean_io_cancel_core(t: b_lean_obj_arg) void;
pub extern fn lean_io_has_finished_core(t: b_lean_obj_arg) bool;
pub extern fn lean_io_wait_any_core(task_list: b_lean_obj_arg) b_lean_obj_res;
pub fn lean_alloc_external(arg_cls: [*c]lean_external_class, arg_data: ?*anyopaque) callconv(.C) ?*lean_object {
    var cls = arg_cls;
    var data = arg_data;
    var o: ?*lean_external_object = @ptrCast(?*lean_external_object, @alignCast(@import("std").meta.alignment(?*lean_external_object), lean_alloc_small_object(@bitCast(c_uint, @truncate(c_uint, @sizeOf(lean_external_object))))));
    lean_set_st_header(@ptrCast(?*lean_object, o), @bitCast(c_uint, @as(c_int, 254)), @bitCast(c_uint, @as(c_int, 0)));
    o.*.m_class = cls;
    o.*.m_data = data;
    return @ptrCast(?*lean_object, o);
}
pub fn lean_get_external_class(arg_o: ?*lean_object) callconv(.C) [*c]lean_external_class {
    var o = arg_o;
    return lean_to_external(o).*.m_class;
}
pub fn lean_get_external_data(arg_o: ?*lean_object) callconv(.C) ?*anyopaque {
    var o = arg_o;
    return lean_to_external(o).*.m_data;
}
pub extern fn lean_nat_big_succ(a: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_add(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_sub(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_mul(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_overflow_mul(a1: usize, a2: usize) ?*lean_object;
pub extern fn lean_nat_big_div(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_mod(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_eq(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_nat_big_le(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_nat_big_lt(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_nat_big_land(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_lor(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_nat_big_xor(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_cstr_to_nat(n: [*c]const u8) lean_obj_res;
pub extern fn lean_big_usize_to_nat(n: usize) lean_obj_res;
pub extern fn lean_big_uint64_to_nat(n: u64) lean_obj_res;
pub fn lean_usize_to_nat(arg_n: usize) callconv(.C) lean_obj_res {
    var n = arg_n;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(n <= (@as(c_ulong, 18446744073709551615) >> @intCast(@import("std").math.Log2Int(c_ulong), 1))))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return lean_box(n) else return lean_big_usize_to_nat(n);
    return null;
}
pub fn lean_unsigned_to_nat(arg_n: c_uint) callconv(.C) lean_obj_res {
    var n = arg_n;
    return lean_usize_to_nat(@bitCast(usize, @as(c_ulong, n)));
}
pub fn lean_uint64_to_nat(arg_n: u64) callconv(.C) lean_obj_res {
    var n = arg_n;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(n <= (@as(c_ulong, 18446744073709551615) >> @intCast(@import("std").math.Log2Int(c_ulong), 1))))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return lean_box(n) else return lean_big_uint64_to_nat(n);
    return null;
}
pub fn lean_nat_succ(arg_a: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return lean_usize_to_nat(lean_unbox(a) +% @bitCast(c_ulong, @as(c_long, @as(c_int, 1)))) else return lean_nat_big_succ(a);
    return null;
}
pub fn lean_nat_add(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return lean_usize_to_nat(lean_unbox(a1) +% lean_unbox(a2)) else return lean_nat_big_add(a1, a2);
    return null;
}
pub fn lean_nat_sub(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n1: usize = lean_unbox(a1);
        var n2: usize = lean_unbox(a2);
        if (n1 < n2) return lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))) else return lean_box(n1 -% n2);
    } else {
        return lean_nat_big_sub(a1, a2);
    }
    return null;
}
pub fn lean_nat_mul(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n1: usize = lean_unbox(a1);
        if (n1 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) return a1;
        var n2: usize = lean_unbox(a2);
        var r: usize = n1 *% n2;
        if ((r <= (@as(c_ulong, 18446744073709551615) >> @intCast(@import("std").math.Log2Int(c_ulong), 1))) and ((r / n1) == n2)) return lean_box(r) else return lean_nat_overflow_mul(n1, n2);
    } else {
        return lean_nat_big_mul(a1, a2);
    }
    return null;
}
pub fn lean_nat_div(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n1: usize = lean_unbox(a1);
        var n2: usize = lean_unbox(a2);
        if (n2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) return lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))) else return lean_box(n1 / n2);
    } else {
        return lean_nat_big_div(a1, a2);
    }
    return null;
}
pub fn lean_nat_mod(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n1: usize = lean_unbox(a1);
        var n2: usize = lean_unbox(a2);
        if (n2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) return lean_box(n1) else return lean_box(n1 % n2);
    } else {
        return lean_nat_big_mod(a1, a2);
    }
    return null;
}
pub fn lean_nat_eq(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return a1 == a2;
    } else {
        return lean_nat_big_eq(a1, a2);
    }
    return false;
}
pub fn lean_nat_dec_eq(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_nat_eq(a1, a2)));
}
pub fn lean_nat_ne(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return !lean_nat_eq(a1, a2);
}
pub fn lean_nat_le(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return a1 <= a2;
    } else {
        return lean_nat_big_le(a1, a2);
    }
    return false;
}
pub fn lean_nat_dec_le(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_nat_le(a1, a2)));
}
pub fn lean_nat_lt(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return a1 < a2;
    } else {
        return lean_nat_big_lt(a1, a2);
    }
    return false;
}
pub fn lean_nat_dec_lt(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_nat_lt(a1, a2)));
}
pub fn lean_nat_land(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return @intToPtr(?*lean_object, @intCast(usize, @ptrToInt(a1)) & @intCast(usize, @ptrToInt(a2)));
    } else {
        return lean_nat_big_land(a1, a2);
    }
    return null;
}
pub fn lean_nat_lor(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return @intToPtr(?*lean_object, @intCast(usize, @ptrToInt(a1)) | @intCast(usize, @ptrToInt(a2)));
    } else {
        return lean_nat_big_lor(a1, a2);
    }
    return null;
}
pub fn lean_nat_lxor(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_box(lean_unbox(a1) ^ lean_unbox(a2));
    } else {
        return lean_nat_big_xor(a1, a2);
    }
    return null;
}
pub extern fn lean_nat_shiftl(a1: b_lean_obj_arg, a2: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_nat_shiftr(a1: b_lean_obj_arg, a2: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_nat_pow(a1: b_lean_obj_arg, a2: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_nat_gcd(a1: b_lean_obj_arg, a2: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_nat_log2(a: b_lean_obj_arg) lean_obj_res;
pub extern fn lean_int_big_neg(a: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_add(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_sub(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_mul(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_div(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_mod(a1: ?*lean_object, a2: ?*lean_object) ?*lean_object;
pub extern fn lean_int_big_eq(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_int_big_le(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_int_big_lt(a1: ?*lean_object, a2: ?*lean_object) bool;
pub extern fn lean_int_big_nonneg(a: ?*lean_object) bool;
pub extern fn lean_cstr_to_int(n: [*c]const u8) ?*lean_object;
pub extern fn lean_big_int_to_int(n: c_int) ?*lean_object;
pub extern fn lean_big_size_t_to_int(n: usize) ?*lean_object;
pub extern fn lean_big_int64_to_int(n: i64) ?*lean_object;
pub fn lean_int_to_int(arg_n: c_int) callconv(.C) lean_obj_res {
    var n = arg_n;
    if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) return lean_box(@bitCast(usize, @as(c_ulong, @bitCast(c_uint, n)))) else if (((if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) -@as(c_int, 2147483647) - @as(c_int, 1) else -(@as(c_int, 1) << @intCast(@import("std").math.Log2Int(c_int), 30))) <= n) and (n <= (if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) @as(c_int, 2147483647) else @as(c_int, 1) << @intCast(@import("std").math.Log2Int(c_int), 30)))) return lean_box(@bitCast(usize, @as(c_ulong, @bitCast(c_uint, n)))) else return lean_big_int_to_int(n);
    return null;
}
pub fn lean_int64_to_int(arg_n: i64) callconv(.C) lean_obj_res {
    var n = arg_n;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@bitCast(c_long, @as(c_long, if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) -@as(c_int, 2147483647) - @as(c_int, 1) else -(@as(c_int, 1) << @intCast(@import("std").math.Log2Int(c_int), 30)))) <= n) and (n <= @bitCast(c_long, @as(c_long, if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) @as(c_int, 2147483647) else @as(c_int, 1) << @intCast(@import("std").math.Log2Int(c_int), 30))))))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return lean_box(@bitCast(usize, @as(c_ulong, @bitCast(c_uint, @bitCast(c_int, @truncate(c_int, n)))))) else return lean_big_int64_to_int(n);
    return null;
}
pub fn lean_scalar_to_int64(arg_a: b_lean_obj_arg) callconv(.C) i64 {
    var a = arg_a;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_scalar(a)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1345), "lean_is_scalar(a)");
        }
    }
    if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) return @bitCast(i64, @as(c_long, @bitCast(c_int, @bitCast(c_uint, @truncate(c_uint, lean_unbox(a)))))) else return @bitCast(i64, @as(c_long, @bitCast(c_int, @truncate(c_uint, @intCast(usize, @ptrToInt(a)))) >> @intCast(@import("std").math.Log2Int(c_int), 1)));
    return 0;
}
pub fn lean_scalar_to_int(arg_a: b_lean_obj_arg) callconv(.C) c_int {
    var a = arg_a;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_is_scalar(a)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1353), "lean_is_scalar(a)");
        }
    }
    if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) return @bitCast(c_int, @bitCast(c_uint, @truncate(c_uint, lean_unbox(a)))) else return @bitCast(c_int, @truncate(c_uint, @intCast(usize, @ptrToInt(a)))) >> @intCast(@import("std").math.Log2Int(c_int), 1);
    return 0;
}
pub fn lean_nat_to_int(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    if (lean_is_scalar(a)) {
        var v: usize = lean_unbox(a);
        if (v <= @bitCast(c_ulong, @as(c_long, if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) @as(c_int, 2147483647) else @as(c_int, 1) << @intCast(@import("std").math.Log2Int(c_int), 30)))) return a else return lean_big_size_t_to_int(v);
    } else {
        return a;
    }
    return null;
}
pub fn lean_int_neg(arg_a: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_int64_to_int(-lean_scalar_to_int64(a));
    } else {
        return lean_int_big_neg(a);
    }
    return null;
}
pub fn lean_int_neg_succ_of_nat(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    var s: lean_obj_res = lean_nat_succ(a);
    lean_dec(a);
    var i: lean_obj_res = lean_nat_to_int(s);
    var r: lean_obj_res = lean_int_neg(i);
    lean_dec(i);
    return r;
}
pub fn lean_int_add(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_int64_to_int(lean_scalar_to_int64(a1) + lean_scalar_to_int64(a2));
    } else {
        return lean_int_big_add(a1, a2);
    }
    return null;
}
pub fn lean_int_sub(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_int64_to_int(lean_scalar_to_int64(a1) - lean_scalar_to_int64(a2));
    } else {
        return lean_int_big_sub(a1, a2);
    }
    return null;
}
pub fn lean_int_mul(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_int64_to_int(lean_scalar_to_int64(a1) * lean_scalar_to_int64(a2));
    } else {
        return lean_int_big_mul(a1, a2);
    }
    return null;
}
pub fn lean_int_div(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) {
            var v1: i64 = @bitCast(i64, @as(c_long, lean_scalar_to_int(a1)));
            var v2: i64 = @bitCast(i64, @as(c_long, lean_scalar_to_int(a2)));
            if (v2 == @bitCast(c_long, @as(c_long, @as(c_int, 0)))) return lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))) else return lean_int64_to_int(@divTrunc(v1, v2));
        } else {
            var v1: c_int = lean_scalar_to_int(a1);
            var v2: c_int = lean_scalar_to_int(a2);
            if (v2 == @as(c_int, 0)) return lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))) else return lean_int_to_int(@divTrunc(v1, v2));
        }
    } else {
        return lean_int_big_div(a1, a2);
    }
    return null;
}
pub fn lean_int_mod(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))) {
            var v1: i64 = lean_scalar_to_int64(a1);
            var v2: i64 = lean_scalar_to_int64(a2);
            if (v2 == @bitCast(c_long, @as(c_long, @as(c_int, 0)))) return a1 else return lean_int64_to_int(@import("std").zig.c_translation.signedRemainder(v1, v2));
        } else {
            var v1: c_int = lean_scalar_to_int(a1);
            var v2: c_int = lean_scalar_to_int(a2);
            if (v2 == @as(c_int, 0)) return a1 else return lean_int_to_int(@import("std").zig.c_translation.signedRemainder(v1, v2));
        }
    } else {
        return lean_int_big_mod(a1, a2);
    }
    return null;
}
pub fn lean_int_eq(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return a1 == a2;
    } else {
        return lean_int_big_eq(a1, a2);
    }
    return false;
}
pub fn lean_int_ne(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return !lean_int_eq(a1, a2);
}
pub fn lean_int_le(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_scalar_to_int(a1) <= lean_scalar_to_int(a2);
    } else {
        return lean_int_big_le(a1, a2);
    }
    return false;
}
pub fn lean_int_lt(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) bool {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt((@as(c_int, @boolToInt(lean_is_scalar(a1))) != 0) and (@as(c_int, @boolToInt(lean_is_scalar(a2))) != 0)))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        return lean_scalar_to_int(a1) < lean_scalar_to_int(a2);
    } else {
        return lean_int_big_lt(a1, a2);
    }
    return false;
}
pub extern fn lean_big_int_to_nat(a: lean_obj_arg) lean_obj_res;
pub fn lean_int_to_nat(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!!lean_int_lt(a, lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))))))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1489), "!lean_int_lt(a, lean_box(0))");
        }
    }
    if (lean_is_scalar(a)) {
        return a;
    } else {
        return lean_big_int_to_nat(a);
    }
    return null;
}
pub fn lean_nat_abs(arg_i: b_lean_obj_arg) callconv(.C) lean_obj_res {
    var i = arg_i;
    if (lean_int_lt(i, lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))))) {
        return lean_int_to_nat(lean_int_neg(i));
    } else {
        lean_inc(i);
        return lean_int_to_nat(i);
    }
    return null;
}
pub fn lean_int_dec_eq(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_int_eq(a1, a2)));
}
pub fn lean_int_dec_le(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_int_le(a1, a2)));
}
pub fn lean_int_dec_lt(arg_a1: b_lean_obj_arg, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @as(u8, @boolToInt(lean_int_lt(a1, a2)));
}
pub fn lean_int_dec_nonneg(arg_a: b_lean_obj_arg) callconv(.C) u8 {
    var a = arg_a;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) return @bitCast(u8, @truncate(i8, @boolToInt(lean_scalar_to_int(a) >= @as(c_int, 0)))) else return @as(u8, @boolToInt(lean_int_big_nonneg(a)));
    return 0;
}
pub fn lean_bool_to_uint64(arg_a: u8) callconv(.C) u64 {
    var a = arg_a;
    return @bitCast(u64, @as(c_ulong, a));
}
pub extern fn lean_uint8_of_big_nat(a: b_lean_obj_arg) u8;
pub fn lean_uint8_of_nat(arg_a: b_lean_obj_arg) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(i8, if (@as(c_int, @boolToInt(lean_is_scalar(a))) != 0) @bitCast(c_int, @as(c_uint, @bitCast(u8, @truncate(u8, lean_unbox(a))))) else @bitCast(c_int, @as(c_uint, lean_uint8_of_big_nat(a)))));
}
pub fn lean_uint8_of_nat_mk(arg_a: lean_obj_arg) callconv(.C) u8 {
    var a = arg_a;
    var r: u8 = lean_uint8_of_nat(a);
    lean_dec(a);
    return r;
}
pub fn lean_uint8_to_nat(arg_a: u8) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_usize_to_nat(@bitCast(usize, @as(c_ulong, a)));
}
pub fn lean_uint8_add(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a1)) + @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint8_sub(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a1)) - @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint8_mul(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a1)) * @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint8_div(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, if (@bitCast(c_int, @as(c_uint, a2)) == @as(c_int, 0)) @as(c_int, 0) else @divTrunc(@bitCast(c_int, @as(c_uint, a1)), @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint8_mod(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, if (@bitCast(c_int, @as(c_uint, a2)) == @as(c_int, 0)) @bitCast(c_int, @as(c_uint, a1)) else @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, a1)), @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint8_land(arg_a: u8, arg_b: u8) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a)) & @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint8_lor(arg_a: u8, arg_b: u8) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a)) | @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint8_xor(arg_a: u8, arg_b: u8) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a)) ^ @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint8_shift_left(arg_a: u8, arg_b: u8) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a)) << @intCast(@import("std").math.Log2Int(c_int), @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, b)), @as(c_int, 8)))));
}
pub fn lean_uint8_shift_right(arg_a: u8, arg_b: u8) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, a)) >> @intCast(@import("std").math.Log2Int(c_int), @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, b)), @as(c_int, 8)))));
}
pub fn lean_uint8_complement(arg_a: u8) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(i8, ~@bitCast(c_int, @as(c_uint, a))));
}
pub fn lean_uint8_modn(arg_a1: u8, arg_a2: b_lean_obj_arg) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a2))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n2: c_uint = @bitCast(c_uint, @truncate(c_uint, lean_unbox(a2)));
        return @bitCast(u8, @truncate(u8, if (n2 == @bitCast(c_uint, @as(c_int, 0))) @bitCast(c_uint, @as(c_uint, a1)) else @bitCast(c_uint, @as(c_uint, a1)) % n2));
    } else {
        return a1;
    }
    return 0;
}
pub fn lean_uint8_log2(arg_a: u8) callconv(.C) u8 {
    var a = arg_a;
    var res: u8 = 0;
    while (@bitCast(c_int, @as(c_uint, a)) >= @as(c_int, 2)) {
        res +%= 1;
        a /= @bitCast(u8, @truncate(i8, @as(c_int, 2)));
    }
    return res;
}
pub fn lean_uint8_dec_eq(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) == @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint8_dec_lt(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) < @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint8_dec_le(arg_a1: u8, arg_a2: u8) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) <= @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint8_to_uint16(arg_a: u8) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @as(c_ushort, a));
}
pub fn lean_uint8_to_uint32(arg_a: u8) callconv(.C) u32 {
    var a = arg_a;
    return @bitCast(u32, @as(c_uint, a));
}
pub fn lean_uint8_to_uint64(arg_a: u8) callconv(.C) u64 {
    var a = arg_a;
    return @bitCast(u64, @as(c_ulong, a));
}
pub extern fn lean_uint16_of_big_nat(a: b_lean_obj_arg) u16;
pub fn lean_uint16_of_nat(arg_a: b_lean_obj_arg) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @truncate(c_short, if (@as(c_int, @boolToInt(lean_is_scalar(a))) != 0) @bitCast(c_int, @as(c_int, @bitCast(i16, @truncate(c_ushort, lean_unbox(a))))) else @bitCast(c_int, @as(c_uint, lean_uint16_of_big_nat(a)))));
}
pub fn lean_uint16_of_nat_mk(arg_a: lean_obj_arg) callconv(.C) u16 {
    var a = arg_a;
    var r: u16 = lean_uint16_of_nat(a);
    lean_dec(a);
    return r;
}
pub fn lean_uint16_to_nat(arg_a: u16) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_usize_to_nat(@bitCast(usize, @as(c_ulong, a)));
}
pub fn lean_uint16_add(arg_a1: u16, arg_a2: u16) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a1)) + @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint16_sub(arg_a1: u16, arg_a2: u16) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a1)) - @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint16_mul(arg_a1: u16, arg_a2: u16) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a1)) * @bitCast(c_int, @as(c_uint, a2))));
}
pub fn lean_uint16_div(arg_a1: u16, arg_a2: u16) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u16, @truncate(c_short, if (@bitCast(c_int, @as(c_uint, a2)) == @as(c_int, 0)) @as(c_int, 0) else @divTrunc(@bitCast(c_int, @as(c_uint, a1)), @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint16_mod(arg_a1: u16, arg_a2: u16) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u16, @truncate(c_short, if (@bitCast(c_int, @as(c_uint, a2)) == @as(c_int, 0)) @bitCast(c_int, @as(c_uint, a1)) else @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, a1)), @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint16_land(arg_a: u16, arg_b: u16) callconv(.C) u16 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a)) & @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint16_lor(arg_a: u16, arg_b: u16) callconv(.C) u16 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a)) | @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint16_xor(arg_a: u16, arg_b: u16) callconv(.C) u16 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a)) ^ @bitCast(c_int, @as(c_uint, b))));
}
pub fn lean_uint16_shift_left(arg_a: u16, arg_b: u16) callconv(.C) u16 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a)) << @intCast(@import("std").math.Log2Int(c_int), @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, b)), @as(c_int, 16)))));
}
pub fn lean_uint16_shift_right(arg_a: u16, arg_b: u16) callconv(.C) u16 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u16, @truncate(c_short, @bitCast(c_int, @as(c_uint, a)) >> @intCast(@import("std").math.Log2Int(c_int), @import("std").zig.c_translation.signedRemainder(@bitCast(c_int, @as(c_uint, b)), @as(c_int, 16)))));
}
pub fn lean_uint16_complement(arg_a: u16) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @truncate(c_short, ~@bitCast(c_int, @as(c_uint, a))));
}
pub fn lean_uint16_modn(arg_a1: u16, arg_a2: b_lean_obj_arg) callconv(.C) u16 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a2))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n2: c_uint = @bitCast(c_uint, @truncate(c_uint, lean_unbox(a2)));
        return @bitCast(u16, @truncate(c_ushort, if (n2 == @bitCast(c_uint, @as(c_int, 0))) @bitCast(c_uint, @as(c_uint, a1)) else @bitCast(c_uint, @as(c_uint, a1)) % n2));
    } else {
        return a1;
    }
    return 0;
}
pub fn lean_uint16_log2(arg_a: u16) callconv(.C) u16 {
    var a = arg_a;
    var res: u16 = 0;
    while (@bitCast(c_int, @as(c_uint, a)) >= @as(c_int, 2)) {
        res +%= 1;
        a /= @bitCast(u16, @truncate(c_short, @as(c_int, 2)));
    }
    return res;
}
pub fn lean_uint16_dec_eq(arg_a1: u16, arg_a2: u16) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) == @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint16_dec_lt(arg_a1: u16, arg_a2: u16) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) < @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint16_dec_le(arg_a1: u16, arg_a2: u16) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(@bitCast(c_int, @as(c_uint, a1)) <= @bitCast(c_int, @as(c_uint, a2)))));
}
pub fn lean_uint16_to_uint8(arg_a: u16) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(u8, a));
}
pub fn lean_uint16_to_uint32(arg_a: u16) callconv(.C) u32 {
    var a = arg_a;
    return @bitCast(u32, @as(c_uint, a));
}
pub fn lean_uint16_to_uint64(arg_a: u16) callconv(.C) u64 {
    var a = arg_a;
    return @bitCast(u64, @as(c_ulong, a));
}
pub extern fn lean_uint32_of_big_nat(a: b_lean_obj_arg) u32;
pub fn lean_uint32_of_nat(arg_a: b_lean_obj_arg) callconv(.C) u32 {
    var a = arg_a;
    return if (@as(c_int, @boolToInt(lean_is_scalar(a))) != 0) @bitCast(u32, @truncate(c_uint, lean_unbox(a))) else lean_uint32_of_big_nat(a);
}
pub fn lean_uint32_of_nat_mk(arg_a: lean_obj_arg) callconv(.C) u32 {
    var a = arg_a;
    var r: u32 = lean_uint32_of_nat(a);
    lean_dec(a);
    return r;
}
pub fn lean_uint32_to_nat(arg_a: u32) callconv(.C) lean_obj_res {
    var a = arg_a;
    return lean_usize_to_nat(@bitCast(usize, @as(c_ulong, a)));
}
pub fn lean_uint32_add(arg_a1: u32, arg_a2: u32) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 +% a2;
}
pub fn lean_uint32_sub(arg_a1: u32, arg_a2: u32) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 -% a2;
}
pub fn lean_uint32_mul(arg_a1: u32, arg_a2: u32) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 *% a2;
}
pub fn lean_uint32_div(arg_a1: u32, arg_a2: u32) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_uint, @as(c_int, 0))) @bitCast(c_uint, @as(c_int, 0)) else a1 / a2;
}
pub fn lean_uint32_mod(arg_a1: u32, arg_a2: u32) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_uint, @as(c_int, 0))) a1 else a1 % a2;
}
pub fn lean_uint32_land(arg_a: u32, arg_b: u32) callconv(.C) u32 {
    var a = arg_a;
    var b = arg_b;
    return a & b;
}
pub fn lean_uint32_lor(arg_a: u32, arg_b: u32) callconv(.C) u32 {
    var a = arg_a;
    var b = arg_b;
    return a | b;
}
pub fn lean_uint32_xor(arg_a: u32, arg_b: u32) callconv(.C) u32 {
    var a = arg_a;
    var b = arg_b;
    return a ^ b;
}
pub fn lean_uint32_shift_left(arg_a: u32, arg_b: u32) callconv(.C) u32 {
    var a = arg_a;
    var b = arg_b;
    return a << @intCast(u5, b % @bitCast(c_uint, @as(c_int, 32)));
}
pub fn lean_uint32_shift_right(arg_a: u32, arg_b: u32) callconv(.C) u32 {
    var a = arg_a;
    var b = arg_b;
    return a >> @intCast(u5, b % @bitCast(c_uint, @as(c_int, 32)));
}
pub fn lean_uint32_complement(arg_a: u32) callconv(.C) u32 {
    var a = arg_a;
    return ~a;
}
pub extern fn lean_uint32_big_modn(a1: u32, a2: b_lean_obj_arg) u32;
pub fn lean_uint32_modn(arg_a1: u32, arg_a2: b_lean_obj_arg) callconv(.C) u32 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a2))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n2: usize = lean_unbox(a2);
        return @bitCast(u32, @truncate(c_uint, if (n2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) @bitCast(c_ulong, @as(c_ulong, a1)) else @bitCast(c_ulong, @as(c_ulong, a1)) % n2));
    } else if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 4)))) {
        return lean_uint32_big_modn(a1, a2);
    } else {
        return a1;
    }
    return 0;
}
pub fn lean_uint32_log2(arg_a: u32) callconv(.C) u32 {
    var a = arg_a;
    var res: u32 = 0;
    while (a >= @bitCast(c_uint, @as(c_int, 2))) {
        res +%= 1;
        a /= @bitCast(c_uint, @as(c_int, 2));
    }
    return res;
}
pub fn lean_uint32_dec_eq(arg_a1: u32, arg_a2: u32) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 == a2)));
}
pub fn lean_uint32_dec_lt(arg_a1: u32, arg_a2: u32) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 < a2)));
}
pub fn lean_uint32_dec_le(arg_a1: u32, arg_a2: u32) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 <= a2)));
}
pub fn lean_uint32_to_uint8(arg_a: u32) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(u8, a));
}
pub fn lean_uint32_to_uint16(arg_a: u32) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @truncate(c_ushort, a));
}
pub fn lean_uint32_to_uint64(arg_a: u32) callconv(.C) u64 {
    var a = arg_a;
    return @bitCast(u64, @as(c_ulong, a));
}
pub fn lean_uint32_to_usize(arg_a: u32) callconv(.C) usize {
    var a = arg_a;
    return @bitCast(usize, @as(c_ulong, a));
}
pub extern fn lean_uint64_of_big_nat(a: b_lean_obj_arg) u64;
pub fn lean_uint64_of_nat(arg_a: b_lean_obj_arg) callconv(.C) u64 {
    var a = arg_a;
    return if (@as(c_int, @boolToInt(lean_is_scalar(a))) != 0) @bitCast(u64, lean_unbox(a)) else lean_uint64_of_big_nat(a);
}
pub fn lean_uint64_of_nat_mk(arg_a: lean_obj_arg) callconv(.C) u64 {
    var a = arg_a;
    var r: u64 = lean_uint64_of_nat(a);
    lean_dec(a);
    return r;
}
pub fn lean_uint64_add(arg_a1: u64, arg_a2: u64) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 +% a2;
}
pub fn lean_uint64_sub(arg_a1: u64, arg_a2: u64) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 -% a2;
}
pub fn lean_uint64_mul(arg_a1: u64, arg_a2: u64) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 *% a2;
}
pub fn lean_uint64_div(arg_a1: u64, arg_a2: u64) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) @bitCast(c_ulong, @as(c_long, @as(c_int, 0))) else a1 / a2;
}
pub fn lean_uint64_mod(arg_a1: u64, arg_a2: u64) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) a1 else a1 % a2;
}
pub fn lean_uint64_land(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return a & b;
}
pub fn lean_uint64_lor(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return a | b;
}
pub fn lean_uint64_xor(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return a ^ b;
}
pub fn lean_uint64_shift_left(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return a << @intCast(u6, b % @bitCast(c_ulong, @as(c_long, @as(c_int, 64))));
}
pub fn lean_uint64_shift_right(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return a >> @intCast(u6, b % @bitCast(c_ulong, @as(c_long, @as(c_int, 64))));
}
pub fn lean_uint64_complement(arg_a: u64) callconv(.C) u64 {
    var a = arg_a;
    return ~a;
}
pub extern fn lean_uint64_big_modn(a1: u64, a2: b_lean_obj_arg) u64;
pub fn lean_uint64_modn(arg_a1: u64, arg_a2: b_lean_obj_arg) callconv(.C) u64 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a2))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n2: usize = lean_unbox(a2);
        return if (n2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) a1 else a1 % n2;
    } else {
        return lean_uint64_big_modn(a1, a2);
    }
    return 0;
}
pub fn lean_uint64_log2(arg_a: u64) callconv(.C) u64 {
    var a = arg_a;
    var res: u64 = 0;
    while (a >= @bitCast(c_ulong, @as(c_long, @as(c_int, 2)))) {
        res +%= 1;
        a /= @bitCast(c_ulong, @as(c_long, @as(c_int, 2)));
    }
    return res;
}
pub fn lean_uint64_dec_eq(arg_a1: u64, arg_a2: u64) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 == a2)));
}
pub fn lean_uint64_dec_lt(arg_a1: u64, arg_a2: u64) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 < a2)));
}
pub fn lean_uint64_dec_le(arg_a1: u64, arg_a2: u64) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 <= a2)));
}
pub extern fn lean_uint64_mix_hash(a1: u64, a2: u64) u64;
pub fn lean_uint64_to_uint8(arg_a: u64) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(u8, a));
}
pub fn lean_uint64_to_uint16(arg_a: u64) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @truncate(c_ushort, a));
}
pub fn lean_uint64_to_uint32(arg_a: u64) callconv(.C) u32 {
    var a = arg_a;
    return @bitCast(u32, @truncate(c_uint, a));
}
pub fn lean_uint64_to_usize(arg_a: u64) callconv(.C) usize {
    var a = arg_a;
    return @bitCast(usize, a);
}
pub extern fn lean_usize_of_big_nat(a: b_lean_obj_arg) usize;
pub fn lean_usize_of_nat(arg_a: b_lean_obj_arg) callconv(.C) usize {
    var a = arg_a;
    return if (@as(c_int, @boolToInt(lean_is_scalar(a))) != 0) lean_unbox(a) else lean_usize_of_big_nat(a);
}
pub fn lean_usize_of_nat_mk(arg_a: lean_obj_arg) callconv(.C) usize {
    var a = arg_a;
    var r: usize = lean_usize_of_nat(a);
    lean_dec(a);
    return r;
}
pub fn lean_usize_add(arg_a1: usize, arg_a2: usize) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 +% a2;
}
pub fn lean_usize_sub(arg_a1: usize, arg_a2: usize) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 -% a2;
}
pub fn lean_usize_mul(arg_a1: usize, arg_a2: usize) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return a1 *% a2;
}
pub fn lean_usize_div(arg_a1: usize, arg_a2: usize) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) @bitCast(c_ulong, @as(c_long, @as(c_int, 0))) else a1 / a2;
}
pub fn lean_usize_mod(arg_a1: usize, arg_a2: usize) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return if (a2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) a1 else a1 % a2;
}
pub fn lean_usize_land(arg_a: usize, arg_b: usize) callconv(.C) usize {
    var a = arg_a;
    var b = arg_b;
    return a & b;
}
pub fn lean_usize_lor(arg_a: usize, arg_b: usize) callconv(.C) usize {
    var a = arg_a;
    var b = arg_b;
    return a | b;
}
pub fn lean_usize_xor(arg_a: usize, arg_b: usize) callconv(.C) usize {
    var a = arg_a;
    var b = arg_b;
    return a ^ b;
}
pub fn lean_usize_shift_left(arg_a: usize, arg_b: usize) callconv(.C) usize {
    var a = arg_a;
    var b = arg_b;
    return a << @intCast(@import("std").math.Log2Int(usize), b % (@sizeOf(usize) *% @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))));
}
pub fn lean_usize_shift_right(arg_a: usize, arg_b: usize) callconv(.C) usize {
    var a = arg_a;
    var b = arg_b;
    return a >> @intCast(@import("std").math.Log2Int(usize), b % (@sizeOf(usize) *% @bitCast(c_ulong, @as(c_long, @as(c_int, 8)))));
}
pub fn lean_usize_complement(arg_a: usize) callconv(.C) usize {
    var a = arg_a;
    return ~a;
}
pub extern fn lean_usize_big_modn(a1: usize, a2: b_lean_obj_arg) usize;
pub fn lean_usize_modn(arg_a1: usize, arg_a2: b_lean_obj_arg) callconv(.C) usize {
    var a1 = arg_a1;
    var a2 = arg_a2;
    if (__builtin_expect(@as(c_long, @boolToInt(lean_is_scalar(a2))), @bitCast(c_long, @as(c_long, @as(c_int, 1)))) != 0) {
        var n2: usize = lean_unbox(a2);
        return if (n2 == @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) a1 else a1 % n2;
    } else {
        return lean_usize_big_modn(a1, a2);
    }
    return 0;
}
pub fn lean_usize_log2(arg_a: usize) callconv(.C) usize {
    var a = arg_a;
    var res: usize = 0;
    while (a >= @bitCast(c_ulong, @as(c_long, @as(c_int, 2)))) {
        res +%= 1;
        a /= @bitCast(c_ulong, @as(c_long, @as(c_int, 2)));
    }
    return res;
}
pub fn lean_usize_dec_eq(arg_a1: usize, arg_a2: usize) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 == a2)));
}
pub fn lean_usize_dec_lt(arg_a1: usize, arg_a2: usize) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 < a2)));
}
pub fn lean_usize_dec_le(arg_a1: usize, arg_a2: usize) callconv(.C) u8 {
    var a1 = arg_a1;
    var a2 = arg_a2;
    return @bitCast(u8, @truncate(i8, @boolToInt(a1 <= a2)));
}
pub fn lean_usize_to_uint32(arg_a: usize) callconv(.C) u32 {
    var a = arg_a;
    return @bitCast(u32, @truncate(c_uint, a));
}
pub fn lean_usize_to_uint64(arg_a: usize) callconv(.C) u64 {
    var a = arg_a;
    return @bitCast(u64, a);
}
pub extern fn lean_float_to_string(a: f64) lean_obj_res;
pub extern fn lean_float_scaleb(a: f64, b: b_lean_obj_arg) f64;
pub extern fn lean_float_isnan(a: f64) u8;
pub extern fn lean_float_isfinite(a: f64) u8;
pub extern fn lean_float_isinf(a: f64) u8;
pub extern fn lean_float_frexp(a: f64) lean_obj_res;
pub fn lean_box_uint32(arg_v: u32) callconv(.C) lean_obj_res {
    var v = arg_v;
    if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 4)))) {
        var r: lean_obj_res = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @truncate(c_uint, @sizeOf(u32))));
        lean_ctor_set_uint32(r, @bitCast(c_uint, @as(c_int, 0)), v);
        return r;
    } else {
        return lean_box(@bitCast(usize, @as(c_ulong, v)));
    }
    return null;
}
pub fn lean_unbox_uint32(arg_o: b_lean_obj_arg) callconv(.C) c_uint {
    var o = arg_o;
    if (@sizeOf(?*anyopaque) == @bitCast(c_ulong, @as(c_long, @as(c_int, 4)))) {
        return lean_ctor_get_uint32(o, @bitCast(c_uint, @as(c_int, 0)));
    } else {
        return @bitCast(c_uint, @truncate(c_uint, lean_unbox(o)));
    }
    return 0;
}
pub fn lean_box_uint64(arg_v: u64) callconv(.C) lean_obj_res {
    var v = arg_v;
    var r: lean_obj_res = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @truncate(c_uint, @sizeOf(u64))));
    lean_ctor_set_uint64(r, @bitCast(c_uint, @as(c_int, 0)), v);
    return r;
}
pub fn lean_unbox_uint64(arg_o: b_lean_obj_arg) callconv(.C) u64 {
    var o = arg_o;
    return lean_ctor_get_uint64(o, @bitCast(c_uint, @as(c_int, 0)));
}
pub fn lean_box_usize(arg_v: usize) callconv(.C) lean_obj_res {
    var v = arg_v;
    var r: lean_obj_res = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @truncate(c_uint, @sizeOf(usize))));
    lean_ctor_set_usize(r, @bitCast(c_uint, @as(c_int, 0)), v);
    return r;
}
pub fn lean_unbox_usize(arg_o: b_lean_obj_arg) callconv(.C) usize {
    var o = arg_o;
    return lean_ctor_get_usize(o, @bitCast(c_uint, @as(c_int, 0)));
}
pub fn lean_box_float(arg_v: f64) callconv(.C) lean_obj_res {
    var v = arg_v;
    var r: lean_obj_res = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @truncate(c_uint, @sizeOf(f64))));
    lean_ctor_set_float(r, @bitCast(c_uint, @as(c_int, 0)), v);
    return r;
}
pub fn lean_unbox_float(arg_o: b_lean_obj_arg) callconv(.C) f64 {
    var o = arg_o;
    return lean_ctor_get_float(o, @bitCast(c_uint, @as(c_int, 0)));
}
pub extern fn lean_dbg_trace(s: lean_obj_arg, @"fn": lean_obj_arg) ?*lean_object;
pub extern fn lean_dbg_sleep(ms: u32, @"fn": lean_obj_arg) ?*lean_object;
pub extern fn lean_dbg_trace_if_shared(s: lean_obj_arg, a: lean_obj_arg) ?*lean_object;
pub extern fn lean_decode_io_error(errnum: c_int, fname: b_lean_obj_arg) lean_obj_res;
pub fn lean_io_mk_world() callconv(.C) lean_obj_res {
    return lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0))));
}
pub fn lean_io_result_is_ok(arg_r: b_lean_obj_arg) callconv(.C) bool {
    var r = arg_r;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(r))) == @as(c_int, 0);
}
pub fn lean_io_result_is_error(arg_r: b_lean_obj_arg) callconv(.C) bool {
    var r = arg_r;
    return @bitCast(c_int, @as(c_uint, lean_ptr_tag(r))) == @as(c_int, 1);
}
pub fn lean_io_result_get_value(arg_r: b_lean_obj_arg) callconv(.C) b_lean_obj_res {
    var r = arg_r;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_io_result_is_ok(r)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1827), "lean_io_result_is_ok(r)");
        }
    }
    return lean_ctor_get(r, @bitCast(c_uint, @as(c_int, 0)));
}
pub fn lean_io_result_get_error(arg_r: b_lean_obj_arg) callconv(.C) b_lean_obj_res {
    var r = arg_r;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!lean_io_result_is_error(r)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1828), "lean_io_result_is_error(r)");
        }
    }
    return lean_ctor_get(r, @bitCast(c_uint, @as(c_int, 0)));
}
pub extern fn lean_io_result_show_error(r: b_lean_obj_arg) void;
pub extern fn lean_io_mark_end_initialization(...) void;
pub fn lean_io_result_mk_ok(arg_a: lean_obj_arg) callconv(.C) lean_obj_res {
    var a = arg_a;
    var r: ?*lean_object = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 0)), @bitCast(c_uint, @as(c_int, 2)), @bitCast(c_uint, @as(c_int, 0)));
    lean_ctor_set(r, @bitCast(c_uint, @as(c_int, 0)), a);
    lean_ctor_set(r, @bitCast(c_uint, @as(c_int, 1)), lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))));
    return r;
}
pub fn lean_io_result_mk_error(arg_e: lean_obj_arg) callconv(.C) lean_obj_res {
    var e = arg_e;
    var r: ?*lean_object = lean_alloc_ctor(@bitCast(c_uint, @as(c_int, 1)), @bitCast(c_uint, @as(c_int, 2)), @bitCast(c_uint, @as(c_int, 0)));
    lean_ctor_set(r, @bitCast(c_uint, @as(c_int, 0)), e);
    lean_ctor_set(r, @bitCast(c_uint, @as(c_int, 1)), lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))));
    return r;
}
pub extern fn lean_mk_io_error_already_exists(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_already_exists_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_eof(lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_hardware_fault(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_illegal_operation(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_inappropriate_type(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_inappropriate_type_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_interrupted(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_invalid_argument(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_invalid_argument_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_no_file_or_directory(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_no_such_thing(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_no_such_thing_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_other_error(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_permission_denied(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_permission_denied_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_protocol_error(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_resource_busy(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_resource_exhausted(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_resource_exhausted_file(lean_obj_arg, u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_resource_vanished(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_time_expired(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_unsatisfied_constraints(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_error_unsupported_operation(u32, lean_obj_arg) lean_obj_res;
pub extern fn lean_mk_io_user_error(str: lean_obj_arg) lean_obj_res;
pub extern fn lean_st_mk_ref(lean_obj_arg, lean_obj_arg) lean_obj_res;
pub extern fn lean_st_ref_get(b_lean_obj_arg, lean_obj_arg) lean_obj_res;
pub extern fn lean_st_ref_set(b_lean_obj_arg, lean_obj_arg, lean_obj_arg) lean_obj_res;
pub extern fn lean_st_ref_reset(b_lean_obj_arg, lean_obj_arg) lean_obj_res;
pub extern fn lean_st_ref_swap(b_lean_obj_arg, lean_obj_arg, lean_obj_arg) lean_obj_res;
pub fn lean_ptr_addr(arg_a: b_lean_obj_arg) callconv(.C) usize {
    var a = arg_a;
    return @intCast(usize, @ptrToInt(a));
}
pub extern fn lean_name_eq(n1: b_lean_obj_arg, n2: b_lean_obj_arg) u8;
pub fn lean_name_hash_ptr(arg_n: b_lean_obj_arg) callconv(.C) u64 {
    var n = arg_n;
    {
        if (__builtin_expect(@bitCast(c_long, @as(c_long, @boolToInt(!!lean_is_scalar(n)))), @bitCast(c_long, @as(c_long, @as(c_int, 0)))) != 0) {
            lean_notify_assert("/home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h", @as(c_int, 1885), "!lean_is_scalar(n)");
        }
    }
    return lean_ctor_get_uint64(n, @bitCast(c_uint, @truncate(c_uint, @sizeOf(?*lean_object) *% @bitCast(c_ulong, @as(c_long, @as(c_int, 2))))));
}
pub fn lean_name_hash(arg_n: b_lean_obj_arg) callconv(.C) u64 {
    var n = arg_n;
    if (lean_is_scalar(n)) return 1723 else return lean_name_hash_ptr(n);
    return 0;
}
pub fn lean_float_to_uint8(arg_a: f64) callconv(.C) u8 {
    var a = arg_a;
    return @bitCast(u8, @truncate(u8, if (0.0 <= a) if (a < 256.0) @bitCast(c_uint, @as(c_uint, @floatToInt(u8, a))) else @as(c_uint, 255) else @bitCast(c_uint, @as(c_int, 0))));
}
pub fn lean_float_to_uint16(arg_a: f64) callconv(.C) u16 {
    var a = arg_a;
    return @bitCast(u16, @truncate(c_ushort, if (0.0 <= a) if (a < 65536.0) @bitCast(c_uint, @as(c_uint, @floatToInt(u16, a))) else @as(c_uint, 65535) else @bitCast(c_uint, @as(c_int, 0))));
}
pub fn lean_float_to_uint32(arg_a: f64) callconv(.C) u32 {
    var a = arg_a;
    return if (0.0 <= a) if (a < 4294967296.0) @floatToInt(u32, a) else @as(c_uint, 4294967295) else @bitCast(c_uint, @as(c_int, 0));
}
pub fn lean_float_to_uint64(arg_a: f64) callconv(.C) u64 {
    var a = arg_a;
    return if (0.0 <= a) if (a < 18446744073709550000.0) @floatToInt(u64, a) else @as(c_ulong, 18446744073709551615) else @bitCast(c_ulong, @as(c_long, @as(c_int, 0)));
}
pub fn lean_float_to_usize(arg_a: f64) callconv(.C) usize {
    var a = arg_a;
    if (@sizeOf(usize) == @sizeOf(u64)) return @bitCast(usize, lean_float_to_uint64(a)) else return @bitCast(usize, @as(c_ulong, lean_float_to_uint32(a)));
    return 0;
}
pub fn lean_float_add(arg_a: f64, arg_b: f64) callconv(.C) f64 {
    var a = arg_a;
    var b = arg_b;
    return a + b;
}
pub fn lean_float_sub(arg_a: f64, arg_b: f64) callconv(.C) f64 {
    var a = arg_a;
    var b = arg_b;
    return a - b;
}
pub fn lean_float_mul(arg_a: f64, arg_b: f64) callconv(.C) f64 {
    var a = arg_a;
    var b = arg_b;
    return a * b;
}
pub fn lean_float_div(arg_a: f64, arg_b: f64) callconv(.C) f64 {
    var a = arg_a;
    var b = arg_b;
    return a / b;
}
pub fn lean_float_negate(arg_a: f64) callconv(.C) f64 {
    var a = arg_a;
    return -a;
}
pub fn lean_float_beq(arg_a: f64, arg_b: f64) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @boolToInt(a == b)));
}
pub fn lean_float_decLe(arg_a: f64, arg_b: f64) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @boolToInt(a <= b)));
}
pub fn lean_float_decLt(arg_a: f64, arg_b: f64) callconv(.C) u8 {
    var a = arg_a;
    var b = arg_b;
    return @bitCast(u8, @truncate(i8, @boolToInt(a < b)));
}
pub fn lean_uint64_to_float(arg_a: u64) callconv(.C) f64 {
    var a = arg_a;
    return @intToFloat(f64, a);
}
pub var l_get__some__fuel___closed__1: ?*lean_object = @import("std").mem.zeroes(?*lean_object);
pub export var fuel_empty: ?*lean_object = @import("std").mem.zeroes(?*lean_object);
pub export var get_some_fuel: ?*lean_object = @import("std").mem.zeroes(?*lean_object); // output.c:30:1: warning: TODO implement translation of stmt class LabelStmtClass
// output.c:29:26: warning: unable to translate function, demoted to extern
pub extern fn fuel_mk_more(arg_x_1: ?*lean_object) ?*lean_object;
pub var l_get__some__fuel___closed__2: ?*lean_object = @import("std").mem.zeroes(?*lean_object); // output.c:22:1: warning: TODO implement translation of stmt class LabelStmtClass
// output.c:21:21: warning: unable to translate function, demoted to extern
pub extern fn _init_fuel_empty() callconv(.C) ?*lean_object; // output.c:39:1: warning: TODO implement translation of stmt class LabelStmtClass
// output.c:38:21: warning: unable to translate function, demoted to extern
pub extern fn _init_l_get__some__fuel___closed__1() callconv(.C) ?*lean_object; // output.c:49:1: warning: TODO implement translation of stmt class LabelStmtClass
// output.c:48:21: warning: unable to translate function, demoted to extern
pub extern fn _init_l_get__some__fuel___closed__2() callconv(.C) ?*lean_object; // output.c:59:1: warning: TODO implement translation of stmt class LabelStmtClass
// output.c:58:21: warning: unable to translate function, demoted to extern
pub extern fn _init_get_some_fuel() callconv(.C) ?*lean_object;
pub extern fn initialize_Init(builtin: u8, ?*lean_object) ?*lean_object;
pub var _G_initialized: bool = @as(c_int, 0) != 0;
pub export fn initialize_lib(arg_builtin: u8, arg_w: ?*lean_object) ?*lean_object {
    var builtin = arg_builtin;
    var w = arg_w;
    _ = @TypeOf(w);
    var res: ?*lean_object = undefined;
    if (_G_initialized) return lean_io_result_mk_ok(lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))));
    _G_initialized = @as(c_int, 1) != 0;
    res = initialize_Init(builtin, lean_io_mk_world());
    if (lean_io_result_is_error(res)) return res;
    lean_dec_ref(res);
    fuel_empty = _init_fuel_empty();
    lean_mark_persistent(fuel_empty);
    l_get__some__fuel___closed__1 = _init_l_get__some__fuel___closed__1();
    lean_mark_persistent(l_get__some__fuel___closed__1);
    l_get__some__fuel___closed__2 = _init_l_get__some__fuel___closed__2();
    lean_mark_persistent(l_get__some__fuel___closed__2);
    get_some_fuel = _init_get_some_fuel();
    lean_mark_persistent(get_some_fuel);
    return lean_io_result_mk_ok(lean_box(@bitCast(usize, @as(c_long, @as(c_int, 0)))));
}
pub const __INTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):80:9
pub const __UINTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):86:9
pub const __FLT16_DENORM_MIN__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):109:9
pub const __FLT16_EPSILON__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):113:9
pub const __FLT16_MAX__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):119:9
pub const __FLT16_MIN__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):122:9
pub const __INT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):183:9
pub const __UINT32_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `U`"); // (no file):205:9
pub const __UINT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):213:9
pub const __seg_gs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):341:9
pub const __seg_fs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):342:9
pub const offsetof = @compileError("unable to translate macro: undefined identifier `__builtin_offsetof`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stddef.h:104:9
pub const __stdint_join3 = @compileError("unable to translate C expr: unexpected token '##'"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:245:9
pub const __int_c_join = @compileError("unable to translate C expr: unexpected token '##'"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:282:9
pub const __uint_c = @compileError("unable to translate macro: undefined identifier `U`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:284:9
pub const __INTN_MIN = @compileError("unable to translate macro: undefined identifier `INT`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:776:10
pub const __INTN_MAX = @compileError("unable to translate macro: undefined identifier `INT`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:777:10
pub const __UINTN_MAX = @compileError("unable to translate macro: undefined identifier `UINT`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:778:9
pub const __INTN_C = @compileError("unable to translate macro: undefined identifier `INT`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:779:10
pub const __UINTN_C = @compileError("unable to translate macro: undefined identifier `UINT`"); // /home/user/.local/share/zig/0.11.0-dev.638+5c67f9ce7/files/lib/include/stdint.h:780:9
pub const LEAN_ALLOCA = @compileError("unable to translate macro: undefined identifier `alloca`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:31:9
pub const LEAN_ALWAYS_INLINE = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:37:9
pub const assert = @compileError("unable to translate macro: undefined identifier `__FILE__`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:49:9
pub const LEAN_EXPORT = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:56:9
pub const LEAN_CASSERT = @compileError("unable to translate macro: undefined identifier `__LINE__`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:88:9
pub const LEAN_impl_PASTE = @compileError("unable to translate C expr: unexpected token '##'"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:90:9
pub const LEAN_impl_CASSERT_LINE = @compileError("unable to translate macro: undefined identifier `assertion_failed_`"); // /home/user/.elan/toolchains/leanprover--lean4---nightly/include/lean/lean.h:91:9
pub const __llvm__ = @as(c_int, 1);
pub const __clang__ = @as(c_int, 1);
pub const __clang_major__ = @as(c_int, 15);
pub const __clang_minor__ = @as(c_int, 0);
pub const __clang_patchlevel__ = @as(c_int, 6);
pub const __clang_version__ = "15.0.6 (https://github.com/ziglang/zig-bootstrap 0ef3eb28c00dd50f4b9dde7844eab4049ee55d92)";
pub const __GNUC__ = @as(c_int, 4);
pub const __GNUC_MINOR__ = @as(c_int, 2);
pub const __GNUC_PATCHLEVEL__ = @as(c_int, 1);
pub const __GXX_ABI_VERSION = @as(c_int, 1002);
pub const __ATOMIC_RELAXED = @as(c_int, 0);
pub const __ATOMIC_CONSUME = @as(c_int, 1);
pub const __ATOMIC_ACQUIRE = @as(c_int, 2);
pub const __ATOMIC_RELEASE = @as(c_int, 3);
pub const __ATOMIC_ACQ_REL = @as(c_int, 4);
pub const __ATOMIC_SEQ_CST = @as(c_int, 5);
pub const __OPENCL_MEMORY_SCOPE_WORK_ITEM = @as(c_int, 0);
pub const __OPENCL_MEMORY_SCOPE_WORK_GROUP = @as(c_int, 1);
pub const __OPENCL_MEMORY_SCOPE_DEVICE = @as(c_int, 2);
pub const __OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = @as(c_int, 3);
pub const __OPENCL_MEMORY_SCOPE_SUB_GROUP = @as(c_int, 4);
pub const __PRAGMA_REDEFINE_EXTNAME = @as(c_int, 1);
pub const __VERSION__ = "Clang 15.0.6 (https://github.com/ziglang/zig-bootstrap 0ef3eb28c00dd50f4b9dde7844eab4049ee55d92)";
pub const __OBJC_BOOL_IS_BOOL = @as(c_int, 0);
pub const __CONSTANT_CFSTRINGS__ = @as(c_int, 1);
pub const __clang_literal_encoding__ = "UTF-8";
pub const __clang_wide_literal_encoding__ = "UTF-32";
pub const __ORDER_LITTLE_ENDIAN__ = @as(c_int, 1234);
pub const __ORDER_BIG_ENDIAN__ = @as(c_int, 4321);
pub const __ORDER_PDP_ENDIAN__ = @as(c_int, 3412);
pub const __BYTE_ORDER__ = __ORDER_LITTLE_ENDIAN__;
pub const __LITTLE_ENDIAN__ = @as(c_int, 1);
pub const _LP64 = @as(c_int, 1);
pub const __LP64__ = @as(c_int, 1);
pub const __CHAR_BIT__ = @as(c_int, 8);
pub const __BOOL_WIDTH__ = @as(c_int, 8);
pub const __SHRT_WIDTH__ = @as(c_int, 16);
pub const __INT_WIDTH__ = @as(c_int, 32);
pub const __LONG_WIDTH__ = @as(c_int, 64);
pub const __LLONG_WIDTH__ = @as(c_int, 64);
pub const __BITINT_MAXWIDTH__ = @as(c_int, 128);
pub const __SCHAR_MAX__ = @as(c_int, 127);
pub const __SHRT_MAX__ = @as(c_int, 32767);
pub const __INT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __LONG_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __LONG_LONG_MAX__ = @as(c_longlong, 9223372036854775807);
pub const __WCHAR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __WCHAR_WIDTH__ = @as(c_int, 32);
pub const __WINT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __WINT_WIDTH__ = @as(c_int, 32);
pub const __INTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTMAX_WIDTH__ = @as(c_int, 64);
pub const __SIZE_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __SIZE_WIDTH__ = @as(c_int, 64);
pub const __UINTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTMAX_WIDTH__ = @as(c_int, 64);
pub const __PTRDIFF_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __PTRDIFF_WIDTH__ = @as(c_int, 64);
pub const __INTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTPTR_WIDTH__ = @as(c_int, 64);
pub const __UINTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTPTR_WIDTH__ = @as(c_int, 64);
pub const __SIZEOF_DOUBLE__ = @as(c_int, 8);
pub const __SIZEOF_FLOAT__ = @as(c_int, 4);
pub const __SIZEOF_INT__ = @as(c_int, 4);
pub const __SIZEOF_LONG__ = @as(c_int, 8);
pub const __SIZEOF_LONG_DOUBLE__ = @as(c_int, 16);
pub const __SIZEOF_LONG_LONG__ = @as(c_int, 8);
pub const __SIZEOF_POINTER__ = @as(c_int, 8);
pub const __SIZEOF_SHORT__ = @as(c_int, 2);
pub const __SIZEOF_PTRDIFF_T__ = @as(c_int, 8);
pub const __SIZEOF_SIZE_T__ = @as(c_int, 8);
pub const __SIZEOF_WCHAR_T__ = @as(c_int, 4);
pub const __SIZEOF_WINT_T__ = @as(c_int, 4);
pub const __SIZEOF_INT128__ = @as(c_int, 16);
pub const __INTMAX_TYPE__ = c_long;
pub const __INTMAX_FMTd__ = "ld";
pub const __INTMAX_FMTi__ = "li";
pub const __UINTMAX_TYPE__ = c_ulong;
pub const __UINTMAX_FMTo__ = "lo";
pub const __UINTMAX_FMTu__ = "lu";
pub const __UINTMAX_FMTx__ = "lx";
pub const __UINTMAX_FMTX__ = "lX";
pub const __PTRDIFF_TYPE__ = c_long;
pub const __PTRDIFF_FMTd__ = "ld";
pub const __PTRDIFF_FMTi__ = "li";
pub const __INTPTR_TYPE__ = c_long;
pub const __INTPTR_FMTd__ = "ld";
pub const __INTPTR_FMTi__ = "li";
pub const __SIZE_TYPE__ = c_ulong;
pub const __SIZE_FMTo__ = "lo";
pub const __SIZE_FMTu__ = "lu";
pub const __SIZE_FMTx__ = "lx";
pub const __SIZE_FMTX__ = "lX";
pub const __WCHAR_TYPE__ = c_int;
pub const __WINT_TYPE__ = c_uint;
pub const __SIG_ATOMIC_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __SIG_ATOMIC_WIDTH__ = @as(c_int, 32);
pub const __CHAR16_TYPE__ = c_ushort;
pub const __CHAR32_TYPE__ = c_uint;
pub const __UINTPTR_TYPE__ = c_ulong;
pub const __UINTPTR_FMTo__ = "lo";
pub const __UINTPTR_FMTu__ = "lu";
pub const __UINTPTR_FMTx__ = "lx";
pub const __UINTPTR_FMTX__ = "lX";
pub const __FLT16_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT16_DIG__ = @as(c_int, 3);
pub const __FLT16_DECIMAL_DIG__ = @as(c_int, 5);
pub const __FLT16_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT16_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT16_MANT_DIG__ = @as(c_int, 11);
pub const __FLT16_MAX_10_EXP__ = @as(c_int, 4);
pub const __FLT16_MAX_EXP__ = @as(c_int, 16);
pub const __FLT16_MIN_10_EXP__ = -@as(c_int, 4);
pub const __FLT16_MIN_EXP__ = -@as(c_int, 13);
pub const __FLT_DENORM_MIN__ = @as(f32, 1.40129846e-45);
pub const __FLT_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT_DIG__ = @as(c_int, 6);
pub const __FLT_DECIMAL_DIG__ = @as(c_int, 9);
pub const __FLT_EPSILON__ = @as(f32, 1.19209290e-7);
pub const __FLT_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT_MANT_DIG__ = @as(c_int, 24);
pub const __FLT_MAX_10_EXP__ = @as(c_int, 38);
pub const __FLT_MAX_EXP__ = @as(c_int, 128);
pub const __FLT_MAX__ = @as(f32, 3.40282347e+38);
pub const __FLT_MIN_10_EXP__ = -@as(c_int, 37);
pub const __FLT_MIN_EXP__ = -@as(c_int, 125);
pub const __FLT_MIN__ = @as(f32, 1.17549435e-38);
pub const __DBL_DENORM_MIN__ = @as(f64, 4.9406564584124654e-324);
pub const __DBL_HAS_DENORM__ = @as(c_int, 1);
pub const __DBL_DIG__ = @as(c_int, 15);
pub const __DBL_DECIMAL_DIG__ = @as(c_int, 17);
pub const __DBL_EPSILON__ = @as(f64, 2.2204460492503131e-16);
pub const __DBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __DBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __DBL_MANT_DIG__ = @as(c_int, 53);
pub const __DBL_MAX_10_EXP__ = @as(c_int, 308);
pub const __DBL_MAX_EXP__ = @as(c_int, 1024);
pub const __DBL_MAX__ = @as(f64, 1.7976931348623157e+308);
pub const __DBL_MIN_10_EXP__ = -@as(c_int, 307);
pub const __DBL_MIN_EXP__ = -@as(c_int, 1021);
pub const __DBL_MIN__ = @as(f64, 2.2250738585072014e-308);
pub const __LDBL_DENORM_MIN__ = @as(c_longdouble, 3.64519953188247460253e-4951);
pub const __LDBL_HAS_DENORM__ = @as(c_int, 1);
pub const __LDBL_DIG__ = @as(c_int, 18);
pub const __LDBL_DECIMAL_DIG__ = @as(c_int, 21);
pub const __LDBL_EPSILON__ = @as(c_longdouble, 1.08420217248550443401e-19);
pub const __LDBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __LDBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __LDBL_MANT_DIG__ = @as(c_int, 64);
pub const __LDBL_MAX_10_EXP__ = @as(c_int, 4932);
pub const __LDBL_MAX_EXP__ = @as(c_int, 16384);
pub const __LDBL_MAX__ = @as(c_longdouble, 1.18973149535723176502e+4932);
pub const __LDBL_MIN_10_EXP__ = -@as(c_int, 4931);
pub const __LDBL_MIN_EXP__ = -@as(c_int, 16381);
pub const __LDBL_MIN__ = @as(c_longdouble, 3.36210314311209350626e-4932);
pub const __POINTER_WIDTH__ = @as(c_int, 64);
pub const __BIGGEST_ALIGNMENT__ = @as(c_int, 16);
pub const __WINT_UNSIGNED__ = @as(c_int, 1);
pub const __INT8_TYPE__ = i8;
pub const __INT8_FMTd__ = "hhd";
pub const __INT8_FMTi__ = "hhi";
pub const __INT8_C_SUFFIX__ = "";
pub const __INT16_TYPE__ = c_short;
pub const __INT16_FMTd__ = "hd";
pub const __INT16_FMTi__ = "hi";
pub const __INT16_C_SUFFIX__ = "";
pub const __INT32_TYPE__ = c_int;
pub const __INT32_FMTd__ = "d";
pub const __INT32_FMTi__ = "i";
pub const __INT32_C_SUFFIX__ = "";
pub const __INT64_TYPE__ = c_long;
pub const __INT64_FMTd__ = "ld";
pub const __INT64_FMTi__ = "li";
pub const __UINT8_TYPE__ = u8;
pub const __UINT8_FMTo__ = "hho";
pub const __UINT8_FMTu__ = "hhu";
pub const __UINT8_FMTx__ = "hhx";
pub const __UINT8_FMTX__ = "hhX";
pub const __UINT8_C_SUFFIX__ = "";
pub const __UINT8_MAX__ = @as(c_int, 255);
pub const __INT8_MAX__ = @as(c_int, 127);
pub const __UINT16_TYPE__ = c_ushort;
pub const __UINT16_FMTo__ = "ho";
pub const __UINT16_FMTu__ = "hu";
pub const __UINT16_FMTx__ = "hx";
pub const __UINT16_FMTX__ = "hX";
pub const __UINT16_C_SUFFIX__ = "";
pub const __UINT16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __INT16_MAX__ = @as(c_int, 32767);
pub const __UINT32_TYPE__ = c_uint;
pub const __UINT32_FMTo__ = "o";
pub const __UINT32_FMTu__ = "u";
pub const __UINT32_FMTx__ = "x";
pub const __UINT32_FMTX__ = "X";
pub const __UINT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __INT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __UINT64_TYPE__ = c_ulong;
pub const __UINT64_FMTo__ = "lo";
pub const __UINT64_FMTu__ = "lu";
pub const __UINT64_FMTx__ = "lx";
pub const __UINT64_FMTX__ = "lX";
pub const __UINT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __INT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST8_TYPE__ = i8;
pub const __INT_LEAST8_MAX__ = @as(c_int, 127);
pub const __INT_LEAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_LEAST8_FMTd__ = "hhd";
pub const __INT_LEAST8_FMTi__ = "hhi";
pub const __UINT_LEAST8_TYPE__ = u8;
pub const __UINT_LEAST8_MAX__ = @as(c_int, 255);
pub const __UINT_LEAST8_FMTo__ = "hho";
pub const __UINT_LEAST8_FMTu__ = "hhu";
pub const __UINT_LEAST8_FMTx__ = "hhx";
pub const __UINT_LEAST8_FMTX__ = "hhX";
pub const __INT_LEAST16_TYPE__ = c_short;
pub const __INT_LEAST16_MAX__ = @as(c_int, 32767);
pub const __INT_LEAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_LEAST16_FMTd__ = "hd";
pub const __INT_LEAST16_FMTi__ = "hi";
pub const __UINT_LEAST16_TYPE__ = c_ushort;
pub const __UINT_LEAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_LEAST16_FMTo__ = "ho";
pub const __UINT_LEAST16_FMTu__ = "hu";
pub const __UINT_LEAST16_FMTx__ = "hx";
pub const __UINT_LEAST16_FMTX__ = "hX";
pub const __INT_LEAST32_TYPE__ = c_int;
pub const __INT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_LEAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_LEAST32_FMTd__ = "d";
pub const __INT_LEAST32_FMTi__ = "i";
pub const __UINT_LEAST32_TYPE__ = c_uint;
pub const __UINT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_LEAST32_FMTo__ = "o";
pub const __UINT_LEAST32_FMTu__ = "u";
pub const __UINT_LEAST32_FMTx__ = "x";
pub const __UINT_LEAST32_FMTX__ = "X";
pub const __INT_LEAST64_TYPE__ = c_long;
pub const __INT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_LEAST64_FMTd__ = "ld";
pub const __INT_LEAST64_FMTi__ = "li";
pub const __UINT_LEAST64_TYPE__ = c_ulong;
pub const __UINT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_LEAST64_FMTo__ = "lo";
pub const __UINT_LEAST64_FMTu__ = "lu";
pub const __UINT_LEAST64_FMTx__ = "lx";
pub const __UINT_LEAST64_FMTX__ = "lX";
pub const __INT_FAST8_TYPE__ = i8;
pub const __INT_FAST8_MAX__ = @as(c_int, 127);
pub const __INT_FAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_FAST8_FMTd__ = "hhd";
pub const __INT_FAST8_FMTi__ = "hhi";
pub const __UINT_FAST8_TYPE__ = u8;
pub const __UINT_FAST8_MAX__ = @as(c_int, 255);
pub const __UINT_FAST8_FMTo__ = "hho";
pub const __UINT_FAST8_FMTu__ = "hhu";
pub const __UINT_FAST8_FMTx__ = "hhx";
pub const __UINT_FAST8_FMTX__ = "hhX";
pub const __INT_FAST16_TYPE__ = c_short;
pub const __INT_FAST16_MAX__ = @as(c_int, 32767);
pub const __INT_FAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_FAST16_FMTd__ = "hd";
pub const __INT_FAST16_FMTi__ = "hi";
pub const __UINT_FAST16_TYPE__ = c_ushort;
pub const __UINT_FAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_FAST16_FMTo__ = "ho";
pub const __UINT_FAST16_FMTu__ = "hu";
pub const __UINT_FAST16_FMTx__ = "hx";
pub const __UINT_FAST16_FMTX__ = "hX";
pub const __INT_FAST32_TYPE__ = c_int;
pub const __INT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_FAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_FAST32_FMTd__ = "d";
pub const __INT_FAST32_FMTi__ = "i";
pub const __UINT_FAST32_TYPE__ = c_uint;
pub const __UINT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_FAST32_FMTo__ = "o";
pub const __UINT_FAST32_FMTu__ = "u";
pub const __UINT_FAST32_FMTx__ = "x";
pub const __UINT_FAST32_FMTX__ = "X";
pub const __INT_FAST64_TYPE__ = c_long;
pub const __INT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_FAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_FAST64_FMTd__ = "ld";
pub const __INT_FAST64_FMTi__ = "li";
pub const __UINT_FAST64_TYPE__ = c_ulong;
pub const __UINT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_FAST64_FMTo__ = "lo";
pub const __UINT_FAST64_FMTu__ = "lu";
pub const __UINT_FAST64_FMTx__ = "lx";
pub const __UINT_FAST64_FMTX__ = "lX";
pub const __USER_LABEL_PREFIX__ = "";
pub const __FINITE_MATH_ONLY__ = @as(c_int, 0);
pub const __GNUC_STDC_INLINE__ = @as(c_int, 1);
pub const __GCC_ATOMIC_TEST_AND_SET_TRUEVAL = @as(c_int, 1);
pub const __CLANG_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __NO_INLINE__ = @as(c_int, 1);
pub const __PIC__ = @as(c_int, 2);
pub const __pic__ = @as(c_int, 2);
pub const __FLT_RADIX__ = @as(c_int, 2);
pub const __DECIMAL_DIG__ = __LDBL_DECIMAL_DIG__;
pub const __GCC_ASM_FLAG_OUTPUTS__ = @as(c_int, 1);
pub const __code_model_small__ = @as(c_int, 1);
pub const __amd64__ = @as(c_int, 1);
pub const __amd64 = @as(c_int, 1);
pub const __x86_64 = @as(c_int, 1);
pub const __x86_64__ = @as(c_int, 1);
pub const __SEG_GS = @as(c_int, 1);
pub const __SEG_FS = @as(c_int, 1);
pub const __k8 = @as(c_int, 1);
pub const __k8__ = @as(c_int, 1);
pub const __tune_k8__ = @as(c_int, 1);
pub const __REGISTER_PREFIX__ = "";
pub const __NO_MATH_INLINES = @as(c_int, 1);
pub const __AES__ = @as(c_int, 1);
pub const __PCLMUL__ = @as(c_int, 1);
pub const __LAHF_SAHF__ = @as(c_int, 1);
pub const __LZCNT__ = @as(c_int, 1);
pub const __RDRND__ = @as(c_int, 1);
pub const __FSGSBASE__ = @as(c_int, 1);
pub const __BMI__ = @as(c_int, 1);
pub const __BMI2__ = @as(c_int, 1);
pub const __POPCNT__ = @as(c_int, 1);
pub const __PRFCHW__ = @as(c_int, 1);
pub const __RDSEED__ = @as(c_int, 1);
pub const __ADX__ = @as(c_int, 1);
pub const __MOVBE__ = @as(c_int, 1);
pub const __FMA__ = @as(c_int, 1);
pub const __F16C__ = @as(c_int, 1);
pub const __FXSR__ = @as(c_int, 1);
pub const __XSAVE__ = @as(c_int, 1);
pub const __XSAVEOPT__ = @as(c_int, 1);
pub const __XSAVEC__ = @as(c_int, 1);
pub const __XSAVES__ = @as(c_int, 1);
pub const __PKU__ = @as(c_int, 1);
pub const __CLFLUSHOPT__ = @as(c_int, 1);
pub const __SGX__ = @as(c_int, 1);
pub const __INVPCID__ = @as(c_int, 1);
pub const __AVX2__ = @as(c_int, 1);
pub const __AVX__ = @as(c_int, 1);
pub const __SSE4_2__ = @as(c_int, 1);
pub const __SSE4_1__ = @as(c_int, 1);
pub const __SSSE3__ = @as(c_int, 1);
pub const __SSE3__ = @as(c_int, 1);
pub const __SSE2__ = @as(c_int, 1);
pub const __SSE2_MATH__ = @as(c_int, 1);
pub const __SSE__ = @as(c_int, 1);
pub const __SSE_MATH__ = @as(c_int, 1);
pub const __MMX__ = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = @as(c_int, 1);
pub const __SIZEOF_FLOAT128__ = @as(c_int, 16);
pub const unix = @as(c_int, 1);
pub const __unix = @as(c_int, 1);
pub const __unix__ = @as(c_int, 1);
pub const linux = @as(c_int, 1);
pub const __linux = @as(c_int, 1);
pub const __linux__ = @as(c_int, 1);
pub const __ELF__ = @as(c_int, 1);
pub const __gnu_linux__ = @as(c_int, 1);
pub const __FLOAT128__ = @as(c_int, 1);
pub const __STDC__ = @as(c_int, 1);
pub const __STDC_HOSTED__ = @as(c_int, 1);
pub const __STDC_VERSION__ = @as(c_long, 201710);
pub const __STDC_UTF_16__ = @as(c_int, 1);
pub const __STDC_UTF_32__ = @as(c_int, 1);
pub const _DEBUG = @as(c_int, 1);
pub const __GCC_HAVE_DWARF2_CFI_ASM = @as(c_int, 1);
pub const __STDDEF_H = "";
pub const __need_ptrdiff_t = "";
pub const __need_size_t = "";
pub const __need_wchar_t = "";
pub const __need_NULL = "";
pub const __need_STDDEF_H_misc = "";
pub const _PTRDIFF_T = "";
pub const _SIZE_T = "";
pub const _WCHAR_T = "";
pub const NULL = @import("std").zig.c_translation.cast(?*anyopaque, @as(c_int, 0));
pub const __CLANG_MAX_ALIGN_T_DEFINED = "";
pub const __STDBOOL_H = "";
pub const __bool_true_false_are_defined = @as(c_int, 1);
pub const @"bool" = bool;
pub const @"true" = @as(c_int, 1);
pub const @"false" = @as(c_int, 0);
pub const __CLANG_STDINT_H = "";
pub const __int_least64_t = i64;
pub const __uint_least64_t = u64;
pub const __int_least32_t = i64;
pub const __uint_least32_t = u64;
pub const __int_least16_t = i64;
pub const __uint_least16_t = u64;
pub const __int_least8_t = i64;
pub const __uint_least8_t = u64;
pub const __uint32_t_defined = "";
pub const __int8_t_defined = "";
pub const __intptr_t_defined = "";
pub const _INTPTR_T = "";
pub const _UINTPTR_T = "";
pub inline fn __int_c(v: anytype, suffix: anytype) @TypeOf(__int_c_join(v, suffix)) {
    return __int_c_join(v, suffix);
}
pub const __int64_c_suffix = __INT64_C_SUFFIX__;
pub const __int32_c_suffix = __INT64_C_SUFFIX__;
pub const __int16_c_suffix = __INT64_C_SUFFIX__;
pub const __int8_c_suffix = __INT64_C_SUFFIX__;
pub inline fn INT64_C(v: anytype) @TypeOf(__int_c(v, __int64_c_suffix)) {
    return __int_c(v, __int64_c_suffix);
}
pub inline fn UINT64_C(v: anytype) @TypeOf(__uint_c(v, __int64_c_suffix)) {
    return __uint_c(v, __int64_c_suffix);
}
pub inline fn INT32_C(v: anytype) @TypeOf(__int_c(v, __int32_c_suffix)) {
    return __int_c(v, __int32_c_suffix);
}
pub inline fn UINT32_C(v: anytype) @TypeOf(__uint_c(v, __int32_c_suffix)) {
    return __uint_c(v, __int32_c_suffix);
}
pub inline fn INT16_C(v: anytype) @TypeOf(__int_c(v, __int16_c_suffix)) {
    return __int_c(v, __int16_c_suffix);
}
pub inline fn UINT16_C(v: anytype) @TypeOf(__uint_c(v, __int16_c_suffix)) {
    return __uint_c(v, __int16_c_suffix);
}
pub inline fn INT8_C(v: anytype) @TypeOf(__int_c(v, __int8_c_suffix)) {
    return __int_c(v, __int8_c_suffix);
}
pub inline fn UINT8_C(v: anytype) @TypeOf(__uint_c(v, __int8_c_suffix)) {
    return __uint_c(v, __int8_c_suffix);
}
pub const INT64_MAX = INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const INT64_MIN = -INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const UINT64_MAX = UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const __INT_LEAST64_MIN = INT64_MIN;
pub const __INT_LEAST64_MAX = INT64_MAX;
pub const __UINT_LEAST64_MAX = UINT64_MAX;
pub const __INT_LEAST32_MIN = INT64_MIN;
pub const __INT_LEAST32_MAX = INT64_MAX;
pub const __UINT_LEAST32_MAX = UINT64_MAX;
pub const __INT_LEAST16_MIN = INT64_MIN;
pub const __INT_LEAST16_MAX = INT64_MAX;
pub const __UINT_LEAST16_MAX = UINT64_MAX;
pub const __INT_LEAST8_MIN = INT64_MIN;
pub const __INT_LEAST8_MAX = INT64_MAX;
pub const __UINT_LEAST8_MAX = UINT64_MAX;
pub const INT_LEAST64_MIN = __INT_LEAST64_MIN;
pub const INT_LEAST64_MAX = __INT_LEAST64_MAX;
pub const UINT_LEAST64_MAX = __UINT_LEAST64_MAX;
pub const INT_FAST64_MIN = __INT_LEAST64_MIN;
pub const INT_FAST64_MAX = __INT_LEAST64_MAX;
pub const UINT_FAST64_MAX = __UINT_LEAST64_MAX;
pub const INT32_MAX = INT32_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal));
pub const INT32_MIN = -INT32_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal)) - @as(c_int, 1);
pub const UINT32_MAX = UINT32_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 4294967295, .decimal));
pub const INT_LEAST32_MIN = __INT_LEAST32_MIN;
pub const INT_LEAST32_MAX = __INT_LEAST32_MAX;
pub const UINT_LEAST32_MAX = __UINT_LEAST32_MAX;
pub const INT_FAST32_MIN = __INT_LEAST32_MIN;
pub const INT_FAST32_MAX = __INT_LEAST32_MAX;
pub const UINT_FAST32_MAX = __UINT_LEAST32_MAX;
pub const INT16_MAX = INT16_C(@as(c_int, 32767));
pub const INT16_MIN = -INT16_C(@as(c_int, 32767)) - @as(c_int, 1);
pub const UINT16_MAX = UINT16_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal));
pub const INT_LEAST16_MIN = __INT_LEAST16_MIN;
pub const INT_LEAST16_MAX = __INT_LEAST16_MAX;
pub const UINT_LEAST16_MAX = __UINT_LEAST16_MAX;
pub const INT_FAST16_MIN = __INT_LEAST16_MIN;
pub const INT_FAST16_MAX = __INT_LEAST16_MAX;
pub const UINT_FAST16_MAX = __UINT_LEAST16_MAX;
pub const INT8_MAX = INT8_C(@as(c_int, 127));
pub const INT8_MIN = -INT8_C(@as(c_int, 127)) - @as(c_int, 1);
pub const UINT8_MAX = UINT8_C(@as(c_int, 255));
pub const INT_LEAST8_MIN = __INT_LEAST8_MIN;
pub const INT_LEAST8_MAX = __INT_LEAST8_MAX;
pub const UINT_LEAST8_MAX = __UINT_LEAST8_MAX;
pub const INT_FAST8_MIN = __INT_LEAST8_MIN;
pub const INT_FAST8_MAX = __INT_LEAST8_MAX;
pub const UINT_FAST8_MAX = __UINT_LEAST8_MAX;
pub const INTPTR_MIN = -__INTPTR_MAX__ - @as(c_int, 1);
pub const INTPTR_MAX = __INTPTR_MAX__;
pub const UINTPTR_MAX = __UINTPTR_MAX__;
pub const PTRDIFF_MIN = -__PTRDIFF_MAX__ - @as(c_int, 1);
pub const PTRDIFF_MAX = __PTRDIFF_MAX__;
pub const SIZE_MAX = __SIZE_MAX__;
pub const INTMAX_MIN = -__INTMAX_MAX__ - @as(c_int, 1);
pub const INTMAX_MAX = __INTMAX_MAX__;
pub const UINTMAX_MAX = __UINTMAX_MAX__;
pub const SIG_ATOMIC_MIN = __INTN_MIN(__SIG_ATOMIC_WIDTH__);
pub const SIG_ATOMIC_MAX = __INTN_MAX(__SIG_ATOMIC_WIDTH__);
pub const WINT_MIN = __UINTN_C(__WINT_WIDTH__, @as(c_int, 0));
pub const WINT_MAX = __UINTN_MAX(__WINT_WIDTH__);
pub const WCHAR_MAX = __WCHAR_MAX__;
pub const WCHAR_MIN = __INTN_MIN(__WCHAR_WIDTH__);
pub inline fn INTMAX_C(v: anytype) @TypeOf(__int_c(v, __INTMAX_C_SUFFIX__)) {
    return __int_c(v, __INTMAX_C_SUFFIX__);
}
pub inline fn UINTMAX_C(v: anytype) @TypeOf(__int_c(v, __UINTMAX_C_SUFFIX__)) {
    return __int_c(v, __UINTMAX_C_SUFFIX__);
}
pub const __CLANG_LIMITS_H = "";
pub const _GCC_LIMITS_H_ = "";
pub const SCHAR_MAX = __SCHAR_MAX__;
pub const SHRT_MAX = __SHRT_MAX__;
pub const INT_MAX = __INT_MAX__;
pub const LONG_MAX = __LONG_MAX__;
pub const SCHAR_MIN = -__SCHAR_MAX__ - @as(c_int, 1);
pub const SHRT_MIN = -__SHRT_MAX__ - @as(c_int, 1);
pub const INT_MIN = -__INT_MAX__ - @as(c_int, 1);
pub const LONG_MIN = -__LONG_MAX__ - @as(c_long, 1);
pub const UCHAR_MAX = (__SCHAR_MAX__ * @as(c_int, 2)) + @as(c_int, 1);
pub const USHRT_MAX = (__SHRT_MAX__ * @as(c_int, 2)) + @as(c_int, 1);
pub const UINT_MAX = (__INT_MAX__ * @as(c_uint, 2)) + @as(c_uint, 1);
pub const ULONG_MAX = (__LONG_MAX__ * @as(c_ulong, 2)) + @as(c_ulong, 1);
pub const MB_LEN_MAX = @as(c_int, 1);
pub const CHAR_BIT = __CHAR_BIT__;
pub const CHAR_MIN = SCHAR_MIN;
pub const CHAR_MAX = __SCHAR_MAX__;
pub const LLONG_MAX = __LONG_LONG_MAX__;
pub const LLONG_MIN = -__LONG_LONG_MAX__ - @as(c_longlong, 1);
pub const ULLONG_MAX = (__LONG_LONG_MAX__ * @as(c_ulonglong, 2)) + @as(c_ulonglong, 1);
pub const LONG_LONG_MAX = __LONG_LONG_MAX__;
pub const LONG_LONG_MIN = -__LONG_LONG_MAX__ - @as(c_longlong, 1);
pub const ULONG_LONG_MAX = (__LONG_LONG_MAX__ * @as(c_ulonglong, 2)) + @as(c_ulonglong, 1);
pub const LEAN_USING_STD = "";
pub const LEAN_VERSION_MAJOR = @as(c_int, 4);
pub const LEAN_VERSION_MINOR = @as(c_int, 0);
pub const LEAN_VERSION_PATCH = @as(c_int, 0);
pub const LEAN_VERSION_IS_RELEASE = @as(c_int, 0);
pub const LEAN_SPECIAL_VERSION_DESC = "nightly-2022-12-05";
pub const LEAN_PACKAGE_VERSION = "NOT-FOUND";
pub const LEAN_SMALL_ALLOCATOR = "";
pub const LEAN_IS_STAGE0 = @as(c_int, 0);
pub const LEAN_CLOSURE_MAX_ARGS = @as(c_int, 16);
pub const LEAN_OBJECT_SIZE_DELTA = @as(c_int, 8);
pub const LEAN_MAX_SMALL_OBJECT_SIZE = @as(c_int, 4096);
pub inline fn LEAN_UNLIKELY(x: anytype) @TypeOf(__builtin_expect(x, @as(c_int, 0))) {
    return __builtin_expect(x, @as(c_int, 0));
}
pub inline fn LEAN_LIKELY(x: anytype) @TypeOf(__builtin_expect(x, @as(c_int, 1))) {
    return __builtin_expect(x, @as(c_int, 1));
}
pub const LEAN_SHARED = "";
pub inline fn LEAN_BYTE(Var: anytype, Index: anytype) @TypeOf((@import("std").zig.c_translation.cast([*c]u8, &Var) + Index).*) {
    return (@import("std").zig.c_translation.cast([*c]u8, &Var) + Index).*;
}
pub const LeanMaxCtorTag = @as(c_int, 244);
pub const LeanClosure = @as(c_int, 245);
pub const LeanArray = @as(c_int, 246);
pub const LeanStructArray = @as(c_int, 247);
pub const LeanScalarArray = @as(c_int, 248);
pub const LeanString = @as(c_int, 249);
pub const LeanMPZ = @as(c_int, 250);
pub const LeanThunk = @as(c_int, 251);
pub const LeanTask = @as(c_int, 252);
pub const LeanRef = @as(c_int, 253);
pub const LeanExternal = @as(c_int, 254);
pub const LeanReserved = @as(c_int, 255);
pub const LEAN_MAX_CTOR_FIELDS = @as(c_int, 256);
pub const LEAN_MAX_CTOR_SCALARS_SIZE = @as(c_int, 1024);
pub const LEAN_MAX_SMALL_NAT = SIZE_MAX >> @as(c_int, 1);
pub const LEAN_MAX_SMALL_INT = if (@import("std").zig.c_translation.sizeof(?*anyopaque) == @as(c_int, 8)) INT_MAX else @as(c_int, 1) << @as(c_int, 30);
pub const LEAN_MIN_SMALL_INT = if (@import("std").zig.c_translation.sizeof(?*anyopaque) == @as(c_int, 8)) INT_MIN else -(@as(c_int, 1) << @as(c_int, 30));
pub const lean_task = struct_lean_task;
