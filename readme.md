## Usage

- Install [Zig](https://ziglang.org/)
- Install Lean 4 with [elan](https://github.com/leanprover/elan/) or download from the official website
- Set LEAN_DIR in Makefile to where you installed Lean
- `make`
- `./main`

