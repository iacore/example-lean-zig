LEAN_DIR ?= /home/${USER}/.elan/toolchains/leanprover--lean4---nightly

CFLAGS = -I ${LEAN_DIR}/include -fPIC
LIBS = -Wl,--as-needed -lgmp -ldl -pthread -lleancpp -lLean -lInit -lleanrt -lc++ -lc++abi -lunwind
LFLAGS = -L ${LEAN_DIR}/lib/lean -L /usr/lib ${LIBS}

main: driver.zig output.c
	zig cc -o main ${CFLAGS} ${LFLAGS} driver.zig output.c
	#Need to use libunwind to replace GNU extension
	#zig cc --target=native-linux-musl -o main ${CFLAGS} ${LFLAGS} driver.zig output.c

output.c: lib.lean
	lean --c=output.c lib.lean

# only for reference
output.zig: output.c
	zig translate-c ${CFLAGS} output.c > output.zig
