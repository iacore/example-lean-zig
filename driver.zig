const std = @import("std");
// const lean = @import("output.zig");

const lean = struct {
    const c = @cImport({
        @cInclude("lean/lean.h");
    });
    
    pub const object = packed struct {
        m_rc: c_int,
        m_cs_sz: u16,
        m_other: u8,
        m_tag: u8,
    };
    pub usingnamespace c;
    pub extern const fuel_empty: ?*c.lean_object;
    pub extern fn fuel_mk_more(arg_x_1: ?*c.lean_object) ?*c.lean_object;
    pub extern fn initialize_lib(arg_builtin: u8, arg_w: ?*c.lean_object) ?*c.lean_object;
    pub extern fn lean_initialize_runtime_module() void;
    // pub extern fn lean_io_mk_world() *lean_object;
    // pub extern fn lean_io_result_is_ok(res: ?*lean_object) bool;
};

test ".empty" {
    const f0 = lean.fuel_empty;
    std.log.debug("{}", .{@ptrToInt(f0)});
}

test ".some .empty" {
    const f0 = lean.fuel_empty;
    const f1 = lean.fuel_mk_more(f0);
    std.log.debug("{}", .{@ptrToInt(f1)});
}

pub fn main() void {
    lean.lean_initialize_runtime_module();

    const res = lean.initialize_lib(1, lean.lean_io_mk_world());
    // if (lean.lean_io_result_is_ok(res)) {
    //     std.log.info("init good",.{});
    // }
    _ = res;
    const f0 = lean.fuel_empty;
    
    std.log.debug("{any} {any}", .{lean.lean_box(0), lean.lean_box(1)});
// LSB=1 means it's not a pointer
// then unbox it to get the value
    std.log.debug("{x:0>16} {}", .{@ptrToInt(f0), lean.lean_unbox(f0)});
    const f1 = lean.fuel_mk_more(f0);
    const f1_ptr = @ptrCast(*lean.object, @alignCast(8, f1));
    std.log.debug("{x:0>16} {}", .{@ptrToInt(f1), f1_ptr.*});
}
