// Lean compiler output
// Module: lib
// Imports: Init
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_get__some__fuel___closed__1;
LEAN_EXPORT lean_object* fuel_empty;
LEAN_EXPORT lean_object* get_some_fuel;
LEAN_EXPORT lean_object* fuel_mk_more(lean_object*);
static lean_object* l_get__some__fuel___closed__2;
static lean_object* _init_fuel_empty() {
_start:
{
lean_object* x_1; 
x_1 = lean_box(0);
return x_1;
}
}
LEAN_EXPORT lean_object* fuel_mk_more(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_get__some__fuel___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_box(0);
x_2 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_get__some__fuel___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_get__some__fuel___closed__1;
x_2 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_get_some_fuel() {
_start:
{
lean_object* x_1; 
x_1 = l_get__some__fuel___closed__2;
return x_1;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_lib(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
fuel_empty = _init_fuel_empty();
lean_mark_persistent(fuel_empty);
l_get__some__fuel___closed__1 = _init_l_get__some__fuel___closed__1();
lean_mark_persistent(l_get__some__fuel___closed__1);
l_get__some__fuel___closed__2 = _init_l_get__some__fuel___closed__2();
lean_mark_persistent(l_get__some__fuel___closed__2);
get_some_fuel = _init_get_some_fuel();
lean_mark_persistent(get_some_fuel);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
